require 'sinatra'
require 'sequel'
require 'psych'
require 'json'
require 'excon'
require 'logger'
require 'bcrypt'
require 'securerandom'
require 'digest/sha2'
require 'sinatra/cors'

if ENV['DATABASE_URL']
  connstr = ENV['DATABASE_URL']
else
  config = Psych.load_file("./config.yml")
  db_config = config['database']
  if db_config['db_username'] or db_config['db_password']
    login = "#{db_config['db_username']}:#{db_config['db_password']}@"
  else
    login = ''
  end
  connstr = "postgres://#{login}#{db_config['db_address']}/#{db_config['db_name']}"
end

Sequel.extension :core_extensions
DB = Sequel.connect(connstr, pool_timeout: 10)
DB.extension(:pagination, :pg_array, :pg_streaming)

require './models/init'
require './controllers/init'

map '/clip' do
  run ClipController
end

map '/fencers' do
  run FencerController
end

map '/reels' do
  run ReelController
end

map '/fantasy' do
  run FantasyController
end

map '/account' do
  run AccountController
end

map '/admin' do
  run AdminController
end

map '/api' do
  run ApiController
end

map '/fencingai' do
  run FencingAiController
end

map '/blog' do
  run BlogController
end

run HomeController
