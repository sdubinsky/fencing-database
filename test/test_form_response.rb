require_relative 'base_test'

class TestFormResponse < BaseTest
  def test_generates_html
    questions = [
      {
        type: :select,
        id: 'score-body-select',
        question: "Where was the hit?",
        note: "If both are hit, respond for the fencer who didn't initiate the action.",
        options: [
          {value: "front_arm", display: "Front arm"},
          {value: "test2", display: "test2"}
        ]
      }
    ]
    result = FormResponse.questions_to_html(questions)
    expected = "<label for=\"score-body-select\" class=\"form-question h4\">Where was the hit?<select class=\"form-control\" name=\"score-body-select\"><option name=\"blank\" value=\"\">No Response</option><option name=\"front_arm\" id=\"front_arm\" value=\"front_arm\">Front arm</option><option name=\"test2\" id=\"test2\" value=\"test2\">test2</option></select></label>"

    assert_equal expected, result
  end
end
