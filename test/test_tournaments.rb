require_relative 'base_test'

class TestTournaments < BaseTest
  def setup
    @tournament = create(:tournament,
                         tournament_id: 'testberne2019',
                         tournament_name: 'Test Bern World Cup',
                         weapon: 'epee',
                         urls: Sequel.pg_array(['https://fie.org/competition/2020/385']))
  end

  def test_gets_entries
    skip 'not needed'
    entries = Tournament.download_entries @tournament.urls[0]
    assert entries.length > 200
  end

  def test_gets_results
    skip 'not needed'
    results = Tournament.download_results @tournament.urls[0]
    assert results.length > 200
  end
end
