require_relative 'base_test'

class TestNormalizeNames < BaseTest
  def setup
    t = create :tournament
    @f = create :fencer, last_name: 'APITHY'
    @f2 = create :fencer, last_name: 'IBRAGIMOV'
    create(:fencers_tournament, tournament_id: t.id, fencer_id: @f.id)
    create(:fencers_tournament, tournament_id: t.id, fencer_id: @f2.id)
  end

  def test_last_name_first_initial
    #there are multiple ibragimovs in the tournament, so this finds just kamil.
    test_gfycat = create(:gfycat,
      fotl_name: 'apithy b',
      fotr_name: 'ibragimov k',
      tournament_id: Tournament.first.tournament_id,
      weapon: 'sabre'
    )

    assert_nil test_gfycat.left_fencer_id
    test_gfycat.normalize_names true

    assert_equal @f.id, test_gfycat.left_fencer_id
    assert_equal @f2.id, test_gfycat.right_fencer_id
  end
end
