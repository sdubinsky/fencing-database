require 'minitest/autorun'
require 'minitest/hooks/test'
require 'rack/test'
require 'sequel'
require 'sequel/core'
require 'sqlite3'
require 'factory_bot'

class BaseTest < Minitest::Test
  connstr = "postgres://deus-ex:123456@localhost:5432/fencing-database-test"
  DB = Sequel.connect(connstr)
  DB.extension :pg_array
  Sequel.extension :migration
  Sequel::Migrator.run(DB, 'db/migrations')
  require_relative '../models/init'
  include FactoryBot::Syntax::Methods
  include Minitest::Hooks
  FactoryBot.find_definitions

  def around
    DB.transaction(rollback: :always) do
      super
    end
  end
end
