require 'json'
require_relative 'base_test'

class TestFantasyTournaments < BaseTest
  def setup
    @tournament = create(:tournament,
                         tournament_id: 'testberne2019',
                         tournament_name: 'Test Bern World Cup',
                         weapon: 'epee',
                         urls: Sequel.pg_array(['https://fie.org/competition/2020/385']),
                         fencers_count: 0)
    entries = JSON.parse(File.read('./test/data/berne2019_entries.json'))
    entries.each do |entry|
      create(:fencers_tournament, tournament_id: @tournament.id, fencer_id: create(:fencer, last_name: entry[0].split[0..-2].join(" "), first_name: entry[0].split[-1], fie_id: entry[4]).id, seed: entry[2].to_i)
    end

    results = JSON.parse(File.read('./test/data/berne2019_results.json'))
    results.each do |result|
      name = result[2..-3].join(' ')
      fencer = Fencer.find_name_possibilities(name, @tournament.id).first
      entry = FencersTournament.first(tournament_id: @tournament.id, fencer_id: fencer.id)

      entry.place = result[0]
      entry.score = result[1]
      entry.save
    end
  end

  def test_entries_list
    entries = FencersTournament.where(tournament_id: @tournament.id).order_by(:seed)
    entries.each_cons(2) do |a, bbb|
      assert a.seed < bbb.seed
    end
  end

  def test_results
    entry_one = Fencer.first(last_name: 'BOREL')
    entry_eleven = Fencer.first(last_name: 'BIDA')
    entry_twenty_one = Fencer.first(last_name: 'KANO')
    submission = create(:fantasy_submission,
                        entry_one: entry_one.id,
                        entry_eleven: entry_eleven.id,
                        entry_twenty_one: entry_twenty_one.id,
                        tournament_id: @tournament.id)

    submission.calculate_score
    assert_equal submission.score, (14 + 8 + 2)
  end
end
