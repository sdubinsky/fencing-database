require_relative 'base_test'

class TestFencer < BaseTest
  include Rack::Test::Methods

  def setup
    t = Tournament.create(
      tournament_name: 'test',
      tournament_id: 'testtournament'
    )
    f = create(:fencer, last_name: 'ibragimov', first_name: 'kamil')
    FencersTournament.create(tournament_id: t.id, fencer_id: f.id)
  end

  def test_find_name_possibilities
    t = Tournament.first

    names = Fencer.find_name_possibilities 'ibragimov k', t.id
    assert_equal 1, names.all.length
  end
end
