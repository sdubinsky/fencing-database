require 'factory_bot'

FactoryBot.define do
  factory :user do
    # thanks to https://leavinsprogramming.blogspot.com/2013/09/using-factorygirl-with-sequel-models.html
    sequence :username do |i|
      "testuser#{i}"
    end
    email { 'test@example.com' }
  end
end
