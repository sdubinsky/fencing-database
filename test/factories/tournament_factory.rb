FactoryBot.define do
  to_create { |i| i.save }
  factory :tournament do
    tournament_id { 'testtournament2022' }
    tournament_name { 'test world cup' }
    start_time { Time.now.to_i + 60 * 60 * 60 }

    transient do
      fencers_count { 2 }
    end

    after(:create) do |t, evaluator|
      1.upto evaluator.fencers_count do |i|
        create(:fencers_tournament, tournament_id: t.id, fencer_id: create(:fencer).id)
      end
    end
  end
end
