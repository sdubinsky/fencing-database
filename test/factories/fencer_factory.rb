require 'factory_bot'

FactoryBot.define do
  factory :fencer do
    to_create { |i| i.save }
    last_name {'test_lname'}
    first_name {'test_fname'}
    fie_id { 0 }
  end
end
