FactoryBot.define do
  factory :gfycat do
    sequence :gfycat_gfy_id do |i|
      "testgfyid#{i}"
    end

    fotl_name { 'lastname firstname' }
    fotr_name {'lastname firstname' }
    weapon { 'epee' }
  end
end
