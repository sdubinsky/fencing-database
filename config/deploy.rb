# config valid only for current version of Capistrano

set :application, 'fencing-database'
set :repo_url, 'git@gitlab.com:sdubinsky/fencing-database.git'
set :deploy_to, '/fencing-database'

set :linked_files, fetch(:linked_files, []).push('config.yml')
set :linked_files, fetch(:linked_files, []).push('session_secret.txt')
set :linked_dirs, fetch(:linked_dirs, []).push('log')
set :branch, 'master'
set :default_env, {
      'BUNDLE_PATH': '/fencing-database/.bundle',
      'ASDF_DATA_DIR': '/opt/asdf'
    }
desc 'migrations'
task 'migrate_release'.to_sym do
  on roles(:web) do
    within release_path do
      execute 'asdf', 'exec', 'bundle', 'exec', 'rake', 'db:migrate'
    end
  end
end

desc 'bundle'
task :bundle do
  on roles(:web) do
    within release_path do
      execute 'asdf', 'exec', 'bundle'
    end
  end
end


namespace :systemd do
  desc 'restart server'
  task :restart do
    on roles(:web) do
      sudo :systemctl, 'restart', 'fencing-database'
    end
  end
end

after 'deploy', 'bundle'
after 'deploy', 'migrate_release'
after 'deploy', 'systemd:restart'
