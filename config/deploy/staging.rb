set stage: :staging
# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

# server 'example.com', user: 'deploy', roles: %w{app db web}, my_property: :my_value
# server 'example.com', user: 'deploy', roles: %w{app web}, other_property: :other_value
# server 'db.example.com', user: 'deploy', roles: %w{db}
server '35.206.93.50', user: 'fencing-database', roles: ['web', 'app']
set :deploy_to, '/fencing-database-staging'
set :branch, 'amber-reskin'

namespace :systemd do
  desc 'restart server'
  task :restart do
    on roles(:web) do
      sudo :systemctl, 'restart', 'fencing-database-staging'
    end
  end
end
