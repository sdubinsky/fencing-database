# Fencing Database Site

## Local Set Up

1. Install a ruby version between 2.5 and 3.
2. `bundle install`
3. Install postgres, if you haven't already, and create yourself a database.
3. Create a config.yml file in the app root dir.  You need a `db` key with the following subkeys:
   1. `db_name`
   2. `db_address`
   3. `db_username`
   4. `db_password`
4. Run the migrations: `rake db:migrate`
5. Add the tournaments.  `download_tournament_entries.rb` has the list of tournament keys.
6. Add the GFYs to the DB as below.
7. If you have any questions, open a ticket or email me.

## Adding new GFYs

1. Add the new tournament to the database.
2. Download the entries via `ruby download_tournament_entries.rb`
2. Run `ruby ./update_gfycat_list.rb`.
   3. Locally, just run the script and it'll use whatever is in your `config.yml`
   4. to update fly:
      * in one terminal, run `fly proxy 5555:5432 -a fencing-database-db`
      * in a second terminal, run `DATABASE_URL=postgres://postgres:E9ejSAUVAK0Vn9Z@localhost:5555/fencing-database ruby download_tournament_entries.rb`
3. run the rake tasks `db:normalize_names` and `db:add_bouts`.

This section is also a rake task named `db:update_gfycats`.

## Interesting SQL queries:
* winning gfycat per bout: `select gfycats.bout_id, gfycat_gfy_id, left_score+right_score as total from gfycats join (select bout_id, max(left_score + right_score) as total from gfycats group by bout_id) x on x.bout_id = gfycats.bout_id and total = x.total;`

* average age of fencer in top 100, plus std dev: `with t_id as(select id from tournaments where tournament_year in ('2023/2024')), fencer_ids as (select fencer_id, sum(score::float) as score from fencers_tournaments  where tournament_id in (select id from t_id) and score is not null group by fencer_id), ages as (select first_name, last_name, gender, birthday, ('2024-03-24'::date - birthday) / 365.0 as age, weapon, score from fencers join fencer_ids on fencers.id = fencer_ids.fencer_id order by score desc limit 100) select weapon, gender, stddev(age)::numeric(50,3) as std_dev, avg(age)::numeric(50,3) as average from ages group by weapon, gender;`
* percentage of non-double touches won:
`with
  all_gfys as (select count(*) as gfy_count, fencers.id as fencer_id from gfycats join fencers on left_fencer_id = fencers.id or right_fencer_id = fencers.id where created_date > 1694696567 and gfycats.weapon='sabre' and status='valid' and touch != 'double' group by fencer_id),
  single_lights as (
     select count(*) as light_count, fencers.id as fencer_id from gfycats join fencers on left_fencer_id=fencers.id or right_fencer_id = fencers.id where created_date > 1694696567 and status='valid' and gfycats.weapon='sabre' and ((left_fencer_id =fencers.id and touch='left') or (right_fencer_id = fencers.id and touch='right')) group by fencer_id)
select (light_count * 1.0 / gfy_count)::numeric(50,3) as pct, last_name, first_name from single_lights join all_gfys on single_lights.fencer_id = all_gfys.fencer_id join fencers on single_lights.fencer_id = fencers.id where gfy_count > 100 order by pct desc limit 10;`
