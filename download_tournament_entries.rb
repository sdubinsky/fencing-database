#takes a url and a tournament id, and adds the data
#to the tournaments_fencers table

#TODO: add tournament arg, save license numbers instead of printing them,
# upload everything to the db, make sure we can upload it to fly

require 'excon'
require 'pry'
require 'sequel'
require 'json'
require 'psych'
Excon.defaults[:middlewares] << Excon::Middleware::RedirectFollower

if ENV['DATABASE_URL']
  connstr = ENV['DATABASE_URL']
else
  config = Psych.load_file("./config.yml")
  db_config = config['database']
  if db_config['db_username'] or db_config['db_password']
    login = "#{db_config['db_username']}:#{db_config['db_password']}@"
  else
    login = ''
  end
  connstr = "postgres://#{login}#{db_config['db_address']}/#{db_config['db_name']}"
end


def download_entries url
  connection = Excon.new url, persistent: true
  page = connection.get
  page = page.body
  lines = page.gsub("\n", "")
  lines = lines.split("<tr>")
  lines = lines.select{|l| l.include? "<td"}
  lines = lines.map{|row| row.split("</td>").map{|entry| entry.gsub(/<.?td.*?>/, "").strip}}
end

url_ids = {
  'budapestsabre2019': ['https://fie.org/competition/2019/160'],
           'bonnfoil2019': ['https://fie.org/competition/2020/135'],
           'budapest2020': ['https://fie.org/competition/2020/112',
                            'https://fie.org/competition/2020/449'],
           'barcelona2020': ['https://fie.org/competition/2020/85'],
           'qatar2020': ['https://fie.org/competition/2020/387',
                         'https://fie.org/competition/2020/79'],
           'tallinn2019': ['https://fie.org/competition/2020/80'],
           'berne2019': ['https://fie.org/competition/2020/385'],
           'budapestwch2019': ['https://fie.org/competition/2019/242',
                               'https://fie.org/competition/2019/241'],
           'dubai2019': ['https://fie.org/competition/2019/451'],
           'cali2019': ['https://fie.org/competition/2019/95',
                        'https://fie.org/competition/2019/113'] ,
           'barcelona2019': ['https://fie.org/competition/2019/85'],
           'bern2018': ['https://fie.org/competition/2019/385'],
           'heidenheim2018': ['https://fie.org/competition/2018/98'],
           'dubai2018': ['https://fie.org/competition/2018/451'],
           'wuxi2018': ['https://fie.org/competition/2018/242',
                        'https://fie.org/competition/2018/241'],
           'barcelona2018': ['https://fie.org/competition/2018/92'],
           'cali2018': ['https://fie.org/competition/2018/95',
                        'https://fie.org/competition/2018/113'],
           'berne2017': ['https://fie.org/competition/2018/385'],
           'doha2017': ['https://fie.org/competition/2018/79',
                        'https://fie.org/competition/2018/387'],
           'heidenheim2017': ['https://fie.org/competition/2017/98'],
           'barcelona2016': ['https://fie.org/competition/2017/85'],
           'heidenheim2019': ['https://fie.org/competition/2019/108'],
           'doha2019': ['https://fie.org/competition/2019/387',
                        'https://fie.org/competition/2019/79'],
           'budapest2019': ['https://fie.org/competition/2019/112',
                            'https://fie.org/competition/2019/449'],
           'montrealsabre2020': ['https://fie.org/competition/2020/152',
                                 'https://fie.org/competition/2020/158'],
           'cairosabre2019': ['https://fie.org/competition/2019/152',
                              'https://fie.org/competition/2019/158'],
           'seoulsabre2019': ['https://fie.org/competition/2019/165',
                              'https://fie.org/competition/2019/468'],
           'cancunsabre2017': ['https://fie.org/competition/2018/152',
                               'https://fie.org/competition/2018/158'],
           'turinfoil2020': ['https://fie.org/competition/2020/134',
                             'https://fie.org/competition/2020/458'],
           'moscowsabre2012': ['https://fie.org/competition/2012/156',
                               'https://fie.org/competition/2012/468'],
           'katowicefoil2019': ['https://fie.org/competition/2019/118'],
           'kazanfoil2020': ['https://fie.org/competition/2020/129'],
           'stpetersburgfoil2019': ['https://fie.org/competition/2019/147'],
           'kazanwch2014': ['https://fie.org/competition/2014/242',
                            'https://fie.org/competition/2014/241'],
           'tunissabre2017': ['https://fie.org/competition/2017/471'],
           'anaheimfoil2019': ['https://fie.org/competition/2019/140',
                               'https://fie.org/competition/2019/121'],
           'anaheimfoil2018': ['https://fie.org/competition/2018/121',
                               'https://fie.org/competition/2018/140'],
           'budapestsabre2021': ['https://fie.org/competition/2021/151',
                                 'https://fie.org/competition/2021/160'],
           'kazanepee2021': ['https://fie.org/competition/2021/92',
                             'https://fie.org/competition/2021/104'],
           'qatarfoil2021': ['https://fie.org/competition/2021/140',
                             'https://fie.org/competition/2021/121'],
           'moscowwch2015': ['https://fie.org/competition/2015/243',
                             'https://fie.org/competition/2015/244',
                             'https://fie.org/competition/2015/241',
                             'https://fie.org/competition/2015/242',
                             'https://fie.org/competition/2015/245',
                             'https://fie.org/competition/2015/246'],
           'tokyoolympics2020': ['https://fie.org/competition/2021/244',
                                 'https://fie.org/competition/2021/245',
                                 'https://fie.org/competition/2021/242',
                                 'https://fie.org/competition/2021/243',
                                 'https://fie.org/competition/2021/241',
                                 'https://fie.org/competition/2021/246'],
           'berneepee2021': ['https://fie.org/competition/2022/385'],
           'orleanssabre2021': ['https://fie.org/competition/2022/152',
                                'https://fie.org/competition/2022/158'],
           'tallinnepee2021': ['https://fie.org/competition/2022/80'],
           'parisfoil2022': ['https://fie.org/competition/2022/142'],
           'tbilisisabre2022': ['https://fie.org/competition/2022/164',
                                'https://fie.org/competition/2022/467'],
           'sochiepee2022': ['https://fie.org/competition/2022/108'],
           'barcelonaepee2022': ['https://fie.org/competition/2022/85'],
           'budapestepee2022': ['https://fie.org/competition/2022/449',
                                'https://fie.org/competition/2022/112'],
           'serbiafoil2022': ['https://fie.org/competition/2022/135',
                              'https://fie.org/competition/2022/125'],
           'tauberbischofsheimfoil2022': ['https://fie.org/competition/2022/122'],
           'cairoepee2022': ['https://fie.org/competition/2022/113',
                             'https://fie.org/competition/2022/95'],
           'tunisiasabre2022': ['https://fie.org/competition/2022/471'],
           'madridsabre2022': ['https://fie.org/competition/2022/474'],
           'incheonfoil2022': ['https://fie.org/competition/2022/457',
                               'https://fie.org/competition/2022/145'],
           'heidenheimepee2022': ['https://fie.org/competition/2022/98'],
           'tbilisiepee2022': ['https://fie.org/competition/2022/104'],
           'paduasabre2022': ['https://fie.org/competition/2022/162',
                              'https://fie.org/competition/2022/676'],
           'eurochamps2022': ['https://fie.org/competition/2022/739',
                              'https://fie.org/competition/2022/742',
                              'https://fie.org/competition/2022/738',
                              'https://fie.org/competition/2022/741',
                              'https://fie.org/competition/2022/740',
                              'https://fie.org/competition/2022/743'],
           'worldchamps2022': ['https://fie.org/competition/2022/244',
                               'https://fie.org/competition/2022/245',
                               'https://fie.org/competition/2022/242',
                               'https://fie.org/competition/2022/243',
                               'https://fie.org/competition/2022/241',
                               'https://fie.org/competition/2022/246'],
           'bernepee2022': ['https://fie.org/competition/2023/385'],
           'bonnfoil2022': ['https://fie.org/competition/2023/1406'],
           'tallinnepee2022': ['https://fie.org/competition/2023/80'],
           'algierssabre2022': ['https://fie.org/competition/2023/1410',
                                'https://fie.org/competition/2023/1408']
}

Sequel.connect connstr do |db|
  db.extension :pg_array
  require './models/init'
  url_ids.each do |tournament_key, urls|
    tournament = Tournament.first(tournament_id: tournament_key.to_s)
    tournament.update(urls: Sequel.pg_array(urls))
    tournament.save
  end
end


# #normal updating
# Sequel.connect connstr do |db|
#   require './models/init'
#   url_ids.reject! do |tournament_key, urls|
#     tournament = Tournament.select(:id, :weapon).first(tournament_id: tournament_key.to_s)
#     raise "Error: No tournament found for id #{tournament_key}" unless tournament
#     old_entries = db[:fencers_tournaments].where(tournament_id: tournament.id).count
#     old_entries > 0
#   end
# end

# url_ids.each do |tournament_key, urls|
#   puts "adding entries from #{tournament_key}"
#   urls.each do |url|
#     entries = download_entries url
#     licenses = entries.map{|entry| entry[4]}
#     Sequel.connect connstr do |db|
#       require './models/init'
#       tournament = Tournament.select(:id, :weapon).first(tournament_id: tournament_key.to_s)
#       new_fencers = entries.select{|entry| Fencer.where(fie_id: entry[4]).count == 0}.map do |entry|
#         [
#           entry[0].split[0],
#           entry[0].split[1],
#           entry[1][0..2],
#           entry[6],
#           tournament.weapon,
#           entry[4],

#         ]
#       end

#       db[:fencers].import([:last_name, :first_name, :nationality, :birthday, :weapon, :fie_id], new_fencers)
#       select = Fencer.select(:id, tournament.id).where(fie_id: licenses)
#       db[:fencers_tournaments].import([:fencers_id, :tournament_id, :seed], select)
#       entries.each do |entry|
#         FencersTournament.first(fencers_id: entry[4], tournament_id: tournament.id).update(seed: entry[2].to_i)
#       end

#     end
#   end
# end

# for adding seeds
# url_ids.each do |tournament_key, urls|
#   puts "processing #{tournament_key}"
#   Sequel.connect connstr do |db|
#     require './models/init'
#     urls.each do |url|
#       entries = download_entries url
#       tournament = Tournament.select(:id, :weapon).first(tournament_id: tournament_key.to_s)
#       entries.each do |entry|
#         fencer = Fencer.first(fie_id: entry[4])
#         seed = entry[2].to_i
#         begin
#           FencersTournament.first(fencers_id: fencer.id, tournament_id: tournament.id).update(seed: seed)b
#         rescue => e
#           begin
#             puts "#{fencer.id} doesn't exist in #{tournament.id}"
#             FencersTournament.insert(tournament_id: tournament.id, fencer_id: fencer.id, seed: seed)
#           rescue
#             puts "#{entry[4]} doesn't exist yet"
#             Fencer.insert(
#               last_name: entry[0].split[0],
#               first_name: entry[0].split[1],
#               nationality: entry[1][0..2],
#               birthday: entry[6],
#               weapon: tournament.weapon,
#               fie_id: entry[4]
#             )
#           end
#         end
#       end
#     end
#     break
#   end
# end
