require 'json'
class Bout < Sequel::Model
  one_to_many :gfycats
  many_to_one :tournament, key: :tournament_id, primary_key: :tournament_id
  one_to_one :left_fencer, primary_key: :left_fencer_id, key: :id, class: :Fencer
  one_to_one :right_fencer, primary_key: :right_fencer_id, key: :id, class: :Fencer
  one_to_one :winning_fencer, primary_key: :winner, key: :id, class: :Fencer

  def final_touch
    return @final_touch if @final_touch
    left_final = gfycats_dataset.exclude(status: "invalid").order_by(:left_score).reverse.first
    right_final = gfycats_dataset.exclude(status: "invalid").order_by(:right_score).reverse.first
    if left_final.left_score < right_final.right_score
      @final_touch = left_final
      return left_final
    else
      @final_touch = right_final
      return right_final
    end
  end

  def winning_score
    if final_touch.left_score < final_touch.right_score
      return final_touch.right_score
    else
      return final_touch.left_score
    end
  end

  def losing_score
    if final_touch.left_score > final_touch.right_score
      return final_touch.right_score
    else
      return final_touch.left_score
    end
  end

  def as_dict
    {
      bout_id: id,
      fotl_last_name: left_fencer.last_name,
      fotl_first_name: left_fencer.first_name,
      fotr_last_name: right_fencer.last_name,
      fotr_first_name: right_fencer.first_name,
      touches: touches,
      tournament_name: tournament.tournament_name,
      tournament_year: tournament.tournament_year,
      tournament_id: tournament.tournament_id
    }
  end

  def touches
    gfycats_dataset.
      select(:left_score, :right_score, :touch, :gfycat_gfy_id, :scored_time).
      where(status: 'valid').
      order_by(:scored_time).
      all.
      map{|gfy| {left_score: gfy[:left_score], right_score: gfy[:right_score], touch: gfy[:touch], gfycat_gfy_id: gfy[:gfycat_gfy_id], scored_time: gfy[:scored_time]}}
  end

  def differential
    (final_touch.left_score - final_touch.right_score).abs
  end

  def to_json
    as_dict.to_json
  end

  def to_s
    "#{left_fencer.name}:#{final_touch.left_score}, #{right_fencer.name}:#{final_touch.right_score}, winner: #{winning_fencer.name}, differential: #{(final_touch.left_score - final_touch.right_score).abs}, tournament: #{tournament.tournament_name}"
  end

  def loser
    ([left_fencer_id, right_fencer_id] - [winner])[0]
  end

  def display
    winning_score = final_touch.left_score > final_touch.right_score ? final_touch.left_score : final_touch.right_score
    losing_score = final_touch.left_score > final_touch.right_score ? final_touch.right_score : final_touch.left_score

    "#{winning_fencer.name} won #{winning_score} - #{final_touch.right_score} at the #{tournament.tournament_year} #{tournament.tournament_name}"
  end

  def self.head_to_head_bouts fencer1, fencer2
    Bout.where(left_fencer_id: fencer1.id, right_fencer_id: fencer2.id).or(left_fencer_id: fencer2.id, right_fencer_id: fencer1.id)
  end

  def self.json
    bouts = Bout.all.map{|bout| bout.as_dict}
    {
      count: bouts.length,
      bouts: bouts
    }.to_json
  end
end
