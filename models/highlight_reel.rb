class HighlightReel < Sequel::Model
  one_to_many :reel_clips
  many_to_one :user
  def export_reel
    ReelClip.where(selected: true, highlight_reel_id: id).map do |clip|
      "wget -O #{clip.gfycat_gfy_id}_raw.mp4 #{clip.url}\n"
    end.join("\n").to_s +
             ReelClip.where(selected: true, highlight_reel_id: id).map do |clip|
      "./add_#{clip.gfycat.touch}_arrow.sh #{clip.gfycat_gfy_id}_raw.mp4"
    end.join("\n").to_s
  end
end
