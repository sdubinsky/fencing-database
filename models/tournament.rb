require 'json'
require 'excon'
require 'pdf-reader'
require 'open-uri'

class Tournament < Sequel::Model
  one_to_many :bouts, key: :tournament_id, primary_key: :tournament_id
  many_to_many :fencers

  # def fencers
  #   left_fencers = Gfycat.select(:left_fencer_id).where(tournament_id: tournament_id)
  #   right_fencers = Gfycat.select(:right_fencer_id).where(tournament_id: tournament_id)
  #   Fencer.where(id: left_fencers).or(id: right_fencers)
  # end

  def as_dict
    {
      id: id,
      short_name: tournament_id,
      name: tournament_name,
      bouts: bouts.map{|b| b.id},
      year: tournament_year
    }
  end

  def to_json
    as_dict.to_json
  end

  def display_name
    "#{tournament_name}, #{tournament_year}"
  end

  def genders
    gender ? [gender] : ['male', 'female']
  end

  def self.json
    tournaments = Tournament.all.map{|tournament| tournament.as_dict}
    {
      count: tournaments.length,
      tournaments: tournaments
    }.to_json
  end

  def self.download_entries url
    entries_suffix = '/entry/pdf?lang=en'
    connection = Excon.new url + entries_suffix
    page = connection.get
    page = page.body
    lines = page.gsub("\n", "")
    lines = lines.split("<tr>")
    lines = lines.select{|l| l.include? "<td"}
    lines = lines.map{|row| row.split("</td>").map{|entry| entry.gsub(/<.?td.*?>/, "").strip}}
  end

  def self.download_results url
    results_suffix = '/results/ranking/pdf'
    pdf = PDF::Reader.new URI.open(url + results_suffix)
    pages = pdf.pages.map do |page|
      page.
        text.
        gsub('\n\n', '\n').
        gsub('\n\n', '\n')
    end.flatten.compact.join("\n")

    pages.lines(chomp: true).map do |line|
      line = line.split
      next unless line[0] =~ /\d/
      score = line[1]
      next unless score
      line
    end.compact
  end
end
