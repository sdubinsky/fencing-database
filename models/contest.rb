class Contest < Sequel::Model
  one_to_many :fantasy_submissions
  one_to_one :tournament
end
