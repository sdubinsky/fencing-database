require 'debug'
class FantasySubmission < Sequel::Model
  many_to_one :tournament
  many_to_one :choice_one, class: :Fencer, key: :entry_one
  many_to_one :choice_eleven, class: :Fencer, key: :entry_eleven
  many_to_one :choice_twenty_one, class: :Fencer, key: :entry_twenty_one
  many_to_one :user

  def calculate_score
    entry_one_score = FencersTournament.first(fencer_id: entry_one, tournament_id: tournament_id).score.to_f
    entry_eleven_score = FencersTournament.first(fencer_id: entry_eleven, tournament_id: tournament_id)&.score.to_f || 0
    entry_twenty_one_score = FencersTournament.first(fencer_id: entry_twenty_one, tournament_id: tournament_id)&.score.to_f || 0
    score = entry_one_score + entry_eleven_score + entry_twenty_one_score
    score = nil unless score > 0
    self.score = score
    save
  end

  def calculate_rarity_weighted_score
    def calculate score, count, total
      score * (1 - (count.to_f) / total)
    end
    entry_one_score = FencersTournament.first(fencer_id: entry_one, tournament_id: tournament_id).score.to_f
    entry_eleven_score = FencersTournament.first(fencer_id: entry_eleven, tournament_id: tournament_id)&.score.to_f || 0
    entry_twenty_one_score = FencersTournament.first(fencer_id: entry_twenty_one, tournament_id: tournament_id)&.score.to_f || 0

    entry_one_count = FantasySubmission.where(entry_one: entry_one, tournament_id: tournament_id).count
    entry_eleven_count = FantasySubmission.where(entry_eleven: entry_eleven, tournament_id: tournament_id).count
    entry_twenty_one_count = FantasySubmission.where(entry_twenty_one: entry_twenty_one, tournament_id: tournament_id).count
    total = FantasySubmission.where(tournament_id: tournament_id).count
    self.rarity_weighted_score = calculate(entry_one_score, entry_one_count, total) +
                            calculate(entry_eleven_score, entry_eleven_count, total) +
                                 calculate(entry_twenty_one_score, entry_twenty_one_count, total)
    save
  end

  def entries
    "#{choice_one.name} | #{choice_eleven.name} | #{choice_twenty_one.name}"
  end
end
