require 'json'
class FormResponse < Sequel::Model
  many_to_one :gfycat, key: :stats_id, primary_key: :gfycat_gfy_id
  many_to_one :user

  def as_dict
    {
      stats_id: stats_id,
      weapon: gfycat.weapon,
      initiated: initiated,
      strip_location: strip_location,
      body_location: body_location,
      created_date: created_date
    }
  end

  def to_json
    as_dict.to_json
  end

  def self.questions weapon
    questions = [
      {
        type: :select,
        id: 'strip-location',
        question: "Where was the hit fencer standing?",
        note: "Use their rear foot to judge. One foot off the back line is still in the warning strip.",
        options: [
          {
            value: "fotl_warning_box",
            display: "FOTL warning box"
          },
          {
            value: "fotl_half",
            display: "FOTL half"
          },
          {
            value: "middle",
            display: "middle"
          },
          {
            value: "fotr_half",
            display: "FOTR half"
          },
          {
            value: "fotr_warning_box",
            display: "FOTR warning box"
          }
        ]
      },
      {
        type: :select,
        id: "initiated-action",
        question: "Who initiated the action?",
        options: [
          {value: "FOTL", display: "FOTL"},
          {value: "FOTR", display: "FOTR"},
          {value: "neither", display: "neither"}
        ]
      },
      {
        type: :select,
        id: "attack-in-prep",
        question: "Was it an attack in prep?",
        options: [
          {value: "yes", display: "Yes"},
          {value: "no", display: "No"},
        ]
      }
    ]
    if weapon == 'sabre'
      questions += [
        {
          type: :select,
          id: 'score-body-select',
          question: "Where was the hit?",
          note: "If both are hit, respond for the fencer who didn't initiate the action.",
          options: [
            {value: "front_arm", display: "Front arm"},
            {value: "torso", display: "Torso"},
            {value: "head", display: "Head"},
            {value: "back_arm", display: "Back arm"},
          ]
        }
      ]
    end

    if weapon == 'epee'
      questions += [
        {
          type: :select,
          id: "epee-attack-tempo",
          question: "What type of attack was it?",
          options: [
            {value: "one", display: "One Tempo"},
            {value: "two", display: "Two Tempo"},
            {value: "three", display: "Remise/Unintentional"}
          ]
        },
        {
          type: :select,
          id: "epee-pull-push",
          question: "Was the scoring fencer pulling or pushing?",
          options: [
            {value: "pull", display: "Pulling"},
            {value: "push", display: "Pushing"}
          ]
        },
        {
          type: :select,
          id: "epee-action-type",
          question: "What was the final action?",
          options: [
            {value: 'lunge', display: 'Lunge'},
            {value: 'fleche', display: 'Fleche'},
            {value: 'parry-riposte', display: 'Parry-Riposte'},
            {value: 'counterattack', display: 'Counterattack'},
            {value: 'remise', display: 'Remise'},
            {value: 'flick', display: 'Flick'},
            {value: 'toe touch', display: 'Toe Touch'}
          ]
        },
        {
          type: :select,
          id: "key-moment",
          question: "What was the key moment?",
          options: [
            {value: "distance-collapse", display: "Collapse of Distance"},
            {value: "instinctual-folly", display: "Instinctual Folly"},
            {value: "punishment", display: "Punishment"},
            {value: "temps-perdu", display: "Temps Perdu (momentary pause)"},
            {value: "technical-error", display: "Technical Error"},
            {value: "strip-position", display: "Strip Positioning"},
          ]
        }
      ]
    end
    questions
  end

  def self.questions_to_html raw_questions
    raw_questions.map do |question|
      output = "<label for=\"#{question[:id]}\" class=\"form-question h4\">#{question[:question]}"
      if question[:type] == :select
        output += "<select class=\"form-control\" name=\"#{question[:id]}\">"
        output += "<option name=\"blank\" value=\"\">No Response</option>"
        question[:options].each do |option|
          output += "<option name=\"#{option[:value]}\" id=\"#{option[:value]}\" value=\"#{option[:value]}\">#{option[:display]}</option>"
        end
        output += "</select>"
      else
        output += "<input type=#{question[:type]} id=#{question[:id]}>"
      end
      output += "</label>"
    end.join
  end

  def self.total filters = {}
    query = build_query filters
    query.count
  end

  def self.most_popular_location filters = {}
    query = build_query filters
    ret = query.select(:strip_location).group_and_count(:strip_location).reverse(:count).limit(1).first
    ret ||= {strip_location: "unknown part", count: 0}
    ret[:strip_location] ||= "unknown part"
    ret
  end

  def self.most_popular_hit filters = {}

    DB["select body_location, count(body_location) as total from form_responses group by body_location order by total desc limit 1;"].first
  end

  def self.heatmap_colors query
    #query must include form_responses
    colors = {}
    heatmap_colors = ['#FFFFFF', '#FFE9E9', '#FFCCCC', '#FF9999', '#FF6666', '#FF3333', '#FF0000', '#CC0000', '#990000', '#660000', '#330000']
    query = query.select(:strip_location).group_and_count(:strip_location)
    total = query.reduce(0){|t, c| t + c[:count]}
    query.each do |location|
      colors[location[:strip_location]] = heatmap_colors[(location[:count].to_f / total * 10).to_i]
    end
    colors
  end

  def self.build_query filters = {}
    query = DB[:gfycats].where(status: 'valid')
    query = query.join(:form_responses, stats_id: :gfycat_gfy_id)
    if filters[:tournament] and filters[:tournament] != "all"
      query = query.where(tournament_id: filters[:tournament])
    end
    if filters[:weapon] and filters[:weapon] != 'all'
      query = query.where(weapon: filters[:weapon])
    end
    if filters[:gender] and filters[:gender] != 'all'
      query = query.where(Sequel.|({gender: filters[:gender]}, {gender: nil}))
    end
    query
  end
end
