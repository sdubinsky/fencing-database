class FencingAiResult < Sequel::Model
  def self.results
    keycodes = FencingAiResult.select(:keycode, :gfycat_gfy_id).group_and_count(:gfycat_gfy_id, :keycode)
    results = {}
    keycodes.each do |keycode|
      results[keycode.gfycat_gfy_id] ||= []
      results[keycode.gfycat_gfy_id] << "#{keycode.keycode}: #{keycode[:count]} results"
    end
    results
  end
end
