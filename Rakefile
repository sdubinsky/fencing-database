require 'json'
require 'excon'
require 'psych'
require 'sequel'
require 'rake/testtask'

def db_address
  if ENV["DATABASE_URL"]
    ENV["DATABASE_URL"]
  else
    config = Psych.load_file("./config.yml")
    db_config = config['database']
    if db_config['db_username'] or db_config['db_password']
      login = "#{db_config['db_username']}:#{db_config['db_password']}@"
    else
      login = ''
    end
    "postgres://#{login}#{db_config['db_address']}/#{db_config['db_name']}"
  end
end

desc 'start the server'
task :server do
  sh 'rerun -b -- bundle exec puma -p 4567 config.ru'
end

desc 'deploy'
task :deploy => [:test] do
  sh 'cap prod deploy'
end

desc 'time between touches'
task :score_times, [:weapon, :gender] do |t, args|
  require 'sequel'
  require 'statistics'

  Sequel.connect db_address do |db|
    require './models/init'
    averages = Bout.exclude(winner: nil).map do |bout|
      if args[:weapon]
        next unless bout.gfycats[0][:weapon ] == args[:weapon]
      end

      if args[:gender]
        next unless bout.gfycats[0][:gender] == args[:gender]
      end
      begin
        times = bout.touches.each_cons(2).map{|a, b| b[:scored_time] - a[:scored_time]}
        times.compact.select{|a| a > 0 && a < 120}
      rescue
        next
      end
    end.compact.flatten
    puts "time: %.2f std dev: %.2f" % [averages.mean, averages.standard_deviation]
  end
end

desc 'format a chapter list'
task :chapters, [:filename, :tournament] do |t, args|
  Sequel.connect db_address do |db|
    require './models/init'
    t = Tournament.first tournament_id: args[:tournament]
    lines = File.readlines(args[:filename])

    lines = lines.map do |line|
      begin
        options = line.split("|")
        fotl = Fencer.find_name_possibilities(options[1], t.id)
        options[1] = fotl.first ? fotl.first.last_name.capitalize : options[1]
        fotr = Fencer.find_name_possibilities(options[2], t.id)
        options[2] = fotr.first ? fotr.first.last_name.capitalize : options[2]
      rescue => e
        require 'debug'; binding.b
      end
      options
    end

    result = lines.
               chunk_while{|a, b| a[1] == b[1] && a[2] == b[2]}.
               to_a.
               map{|a| a[0]}.
               map{|l| "#{l[0]}: #{l[1]} vs. #{l[2]}"}.
               join("\n").
               gsub("\n\n", "\n")
    File.new("#{args[:filename]}_done.txt", 'w').write(result)
  end
end

Rake::TestTask.new(:test) do |t|
  t.libs << "test"
  t.test_files =FileList['test/test*.rb']
  t.warning = false
end


def confidence_interval wins, losses, confidence_level
  sample_size = wins + losses * 1.0
  win_pct = wins / sample_size
  loss_pct = losses / sample_size
  z = 1.65 if confidence_level == 90
  z = 1.96 if confidence_level == 95
  z = 2.58 if confidence_level == 99
  error_bound = z * (Math.sqrt(win_pct * loss_pct / sample_size))

  [(win_pct + error_bound).truncate(2), (win_pct - error_bound).truncate(2)]
end
