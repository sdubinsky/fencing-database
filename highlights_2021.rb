require 'sequel'
connstr = ENV['DATABASE_URL']
Sequel.extension :core_extensions
DB = Sequel.connect(connstr)
DB.extension(:pagination)
require './models/init'

gfycats = Gfycat.where(Sequel.like(:tournament_id, "%2021%")).where(weapon: 'epee')
reel = HighlightReel.first(title: "Best Hits 2021")

DB.transaction do
  gfycats.each do |gfy|
    ReelClip.create(
      gfycat_gfy_id: gfy[:gfycat_gfy_id],
      highlight_reel: reel,
    )
  end
end
