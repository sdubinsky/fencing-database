Sequel.migration do
  change do
    create_table :fantasy_submissions do
      primary_key :id
      foreign_key :user_id
      foreign_key :tournament_id
      foreign_key :entry_one, :fencers
      foreign_key :entry_eleven, :fencers
      foreign_key :entry_twenty_one, :fencers
      Integer :score
      String :gender
      Integer :created_at
    end
  end
end
