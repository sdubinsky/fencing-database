Sequel.migration do
  change do
    alter_table :error_reports do
      add_column :resolution, String
    end
  end
end
