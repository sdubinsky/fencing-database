Sequel.migration do
  change do
    alter_table :form_responses do
      add_column :additional_questions, :jsonb
    end
  end
end
