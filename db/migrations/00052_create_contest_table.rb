Sequel.migration do
  change do
    create_table :contests do
      primary_key :id
      foreign_key :tournament_id
    end
  end
end
