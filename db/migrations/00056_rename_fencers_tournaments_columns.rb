Sequel.migration do
  change do
    alter_table :fencers_tournaments do
      rename_column :fencers_id, :fencer_id
      rename_column :tournaments_id, :tournament_id
    end
  end
end
