Sequel.migration do
  change do
    alter_table :tournaments do
      add_column :start_time, Integer
      add_column :gender, String
    end
  end
end
