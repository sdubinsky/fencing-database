Sequel.migration do
  change do
    create_table :fencing_ai_clips do
      primary_key :id
      String :touch
      String :gfycat_gfy_id
    end
  end
end
