Sequel.migration do
  up do
    alter_table :fencers_tournaments do
      drop_column :score
      add_column :score, String
    end
  end

  down do
    alter_table :fencers_tournaments do
      drop_column :score
      add_column :score, Integer
    end
  end
end
