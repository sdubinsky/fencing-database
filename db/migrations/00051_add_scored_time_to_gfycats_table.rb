Sequel.migration do
  change do
    alter_table :gfycats do
      add_column :scored_time, :integer
    end
  end
end
