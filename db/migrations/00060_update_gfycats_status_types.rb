Sequel.migration do
  up do
    alter_table :gfycats do
      add_column :status, String
    end
    from(:gfycats).where(valid: true).update(status: 'valid')
    alter_table :gfycats do
      drop_column :valid
    end
  end

  down do
    alter_table :gfycats do
      add_column :valid, Boolean
    end
    from(:gfycats).where(status: 'valid').update(valid: true)
    alter_table :gfycats do
      drop_column :status
    end
  end
end
