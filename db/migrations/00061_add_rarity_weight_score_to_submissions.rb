Sequel.migration do
  change do
    alter_table :fantasy_submissions do
      add_column :rarity_weighted_score, Integer
    end
  end
end
