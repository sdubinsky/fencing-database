Sequel.migration do
  change do
    alter_table :fencers_tournaments do
      add_column :seed, Integer
      add_column :place, Integer
      add_column :score, Integer
    end
  end
end
