Sequel.migration do
  up do
    alter_table :tournaments do
      add_column :urls, "text[]"
    end
  end

  down do
    alter_table :tournaments do
      drop_column :urls
    end
  end
end
