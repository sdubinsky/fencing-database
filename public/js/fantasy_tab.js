function open_tab(tab_class, caller) {
    for(const i of document.getElementsByClassName("leaderboard")) {
        i.style.display = "none";
        i.style.border = "none";
    }

    for (const i of document.getElementsByClassName("leaderboard-subheader")) {
        i.style.border = "none";
        i.style.borderBottom = "1px solid";
    }

    var elem = document.getElementsByClassName(tab_class)[0];
    elem.style.display = "grid";
    caller.style.border = "1px solid";
}

function open_comp_tab(tab_class, caller) {
    for(const i of document.getElementsByClassName("competition")) {
        i.style.display = "none";
        i.style.border = "none";
    }

    for (const i of document.getElementsByClassName("comp-subheader")) {
        i.style.border = "none";
        i.style.borderBottom = "1px solid";
    }

    var elem = document.getElementsByClassName(tab_class)[0];
    elem.style.display = "grid";
    caller.style.border = "1px solid";

}
