function submitResult(keycode) {
    var clipId = document.getElementById("clip-id-div").className;
    var body = JSON.stringify({
        "clipId": clipId,
        "keycode": keycode
    });
    var request = new XMLHttpRequest();
    //This is a sync request because otherwise the page reloads too quickly
    request.open('POST', '/fencingai/submit', false);
    request.setRequestHeader("Content-Type", "application/json");
    request.send(body);
}

function listener(e) {
    result = window.codes[e.code];
    if (result) {
        submitResult(e.code);
        location.reload(true);
    }
}
document.addEventListener('keydown', listener);
