var show_hide_hamburger = function () {
    var helper = document.getElementById("hamburger-helper");
    var links = document.getElementById("header-links");
    if (!helper.checked){
        links.style.display = "grid";
        helper.checked = true;
    }else{
        links.style.display = "none";
        helper.checked = false;
    }
}

document.getElementById("hamburger-menu").onclick = show_hide_hamburger;
