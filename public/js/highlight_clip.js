function nextClip(){
    const urlParams = new URLSearchParams(window.location.search);
    var next_clip = document.getElementById("next-clip").innerHTML;
    //gfy here is the preloaded gfy
    urlParams.set("gfy", next_clip);
    window.location.search = urlParams;
}

function submitResult(result, clipId){
    var reelId = document.getElementById("reel-id-div").className;
    var body = JSON.stringify({
        "reelId": reelId,
        "clipId": clipId,
        "result": result
    });
    var request = new XMLHttpRequest();
    //This is a sync request because otherwise the page reloads too quickly
    request.open('POST', '/reels/submit', false);
    request.setRequestHeader("Content-Type", "application/json");
    request.send(body);
}

function newClip(){
    var reelId = document.getElementById("reel-id-div").className;
    var request = new XMLHttpRequest();

    request.open('GET', '/reels/' + reelId + '/next', false);
    result = request.send(body);
    return result;
}

function rejectClip(e) {
    if (e.code === "ArrowLeft" || e.code === "KeyN"){
        submitResult("reject");
        nextClip();
    }
}

function skipClip(e) {
    if (e.code === "ArrowDown") {
        nextClip();
    }
}

function acceptClip(e) {
    if (e.code === "ArrowRight" || e.code === "KeyY"){
        submitResult("accept");
        nextClip();
    }
}

function updateClip(clipId) {
    var clipId = document.getElementById("video-" + clipId);
    var newClipId = newId();

    //TODO
    clipId.source = newClip.source;
    clipId.id = "video-" + newClip.id;
}

document.addEventListener("keydown", rejectClip);
document.addEventListener("keydown", acceptClip);
document.addEventListener("keydown", skipClip);
