require 'pry'
require 'json'
require 'excon'
require 'logger'
require 'sequel'
#real helpers at the bottom - the rest are just some methods from app.rb that didn't fit there.
module Helpers
  def self.get_touches_query_gfycats db, params
    logger = Logger.new($stdout)
    fencer_query = Fencer.select(:id)

    if params["lastname"] and not params["lastname"].empty?
      last_name = params['lastname'].gsub("-", "%").gsub("\w", "%")
      fencer_query = fencer_query.where(Sequel.like(:last_name, last_name.upcase))
    end

    if params["firstname"] and not params["firstname"].empty?
      fencer_query = fencer_query.where(first_name: params["firstname"].capitalize)
    end

    if params['weapon'] and params['weapon'] != 'all'
      fencer_query = fencer_query.where(weapon: params["weapon"])
    end

    if params['gender'] and params['gender'] != 'all'
      fencer_query = fencer_query.where(gender: params["gender"])
    end

    if params['double']
      doubles_arr = ['double']
    else
      doubles_arr = []
    end

    #use min because the gfy with the lowest opponent's score is the one they scored on.
    if params['score-fencer'] == 'highest'
      left_gfys = Gfycat.select(:gfycat_gfy_id, :bout_id, :left_score, :right_score).qualify.join(
        Gfycat.select(:bout_id, :left_score,
                      Sequel.function(:min, :right_score).as(:right_score))
          .qualify.join(
            Gfycat.distinct
              .select(:bout_id, Sequel.function(:max, :left_score).as(:left_score))
              .where(touch: ['left'] + doubles_arr, status: 'valid')
              .group_by(:bout_id)
              .order_by(:left_score), bout_id: :bout_id, left_score: :left_score)
          .where(status: 'valid')
          .group_by(:bout_id, :left_score).qualify,
        bout_id: :bout_id, left_score: :left_score, right_score: :right_score)
                    .qualify.where(status: 'valid', touch: ['left'] + doubles_arr)

      right_gfys = Gfycat.select(:gfycat_gfy_id, :bout_id, :right_score, :right_score).qualify.join(
        Gfycat.select(:bout_id, :right_score,
                      Sequel.function(:min, :left_score).as(:left_score))
          .qualify.join(
            Gfycat.distinct
              .select(:bout_id, Sequel.function(:max, :right_score).as(:right_score))
              .where(touch: ['right'] + doubles_arr, status: 'valid')
              .group_by(:bout_id)
              .order_by(:right_score), bout_id: :bout_id, right_score: :right_score)
          .where(status: 'valid')
          .group_by(:bout_id, :right_score).qualify,
        bout_id: :bout_id, left_score: :left_score, right_score: :right_score)
                     .qualify.where(status: 'valid', touch: ['right'] + doubles_arr)

    elsif params["score-fencer"] and params['score-fencer'] != 'all'
      left_gfys = Gfycat.select(:gfycat_gfy_id, :bout_id, :left_score, :right_score).where(left_score: params['score-fencer'].to_i, touch: ['left'] + doubles_arr, status: 'valid')

      right_gfys = Gfycat.select(:gfycat_gfy_id, :bout_id, :left_score, :right_score).where(right_score: params['score-fencer'].to_i, touch: ['right'] + doubles_arr, status: 'valid')
    else
      left_gfys = Gfycat.select(:gfycat_gfy_id, :bout_id, :left_score, :right_score).where(status: 'valid', touch: ['left'] + doubles_arr)
      right_gfys = Gfycat.select(:gfycat_gfy_id, :bout_id, :right_score, :right_score).where(status: 'valid', touch: ['right'] + doubles_arr)
    end

    if params["tournament"] and params['tournament'] != "all"
      left_gfys = left_gfys.where(tournament_id: params["tournament"])
      right_gfys = right_gfys.where(tournament_id: params["tournament"])
    end

    if params['event-type'] and params['event-type'] != 'all'
      left_gfys = left_gfys.where(event_type: params["event-type"])
      right_gfys = right_gfys.where(event_type: params["event-type"])
    end

    if params['year'] and params['year'] != 'all'
      tournaments = Tournament.select(:tournament_id).where(tournament_year: params['year'])
      left_gfys = left_gfys.where(tournament_id: tournaments)
      right_gfys = right_gfys.where(tournament_id: tournaments)
    end

    if params['location'] and params['location'] != 'all'
      locations = FormResponse.select(:stats_id).where(strip_location: params['location'])
      left_gfys = left_gfys.where(gfycat_gfy_id: locations)
      right_gfys = right_gfys.where(gfycat_gfy_id: locations)
    end

    if params['opposing-lastname'] and !params['opposing-lastname'].empty?
      opposing_last_name = params['opposing-lastname'].gsub("-", "%").gsub("\w", "%")
      opponents = Fencer.where(Sequel.like(:last_name, opposing_last_name.upcase)).select(:id)
      left_gfys = left_gfys.where(right_fencer_id: opponents)
      right_gfys = right_gfys.where(left_fencer_id: opponents)
    end

    left_query = left_gfys.join(fencer_query, id: :left_fencer_id)
    right_query = right_gfys.join(fencer_query, id: :right_fencer_id)
    left_query = left_query.distinct.select(:gfycat_gfy_id)
    right_query = right_query.distinct.select(:gfycat_gfy_id)

    final_query = left_query.union(right_query)
    final_query = Gfycat.select(:gfycat_gfy_id, :tournament_id).where(gfycat_gfy_id: final_query)
    unless params["page"] and params["page"].to_i == -1
      page = (params["page"] || 1).to_i
      final_query = final_query.paginate(page, 10)
    end
    logger.info final_query.sql
    final_query
  end
end


helpers do
  def logged_in?
    !!session[:user_id]
  end

  def login_check
    logger.info "checking login for #{session[:user_id]}"
    if not logged_in?
      logger.info "login failed"
      redirect "/login?url=#{request.path_info}"
    end
    logger.info "login succeeded"
  end

  def admin_check
    redirect '/' unless logged_in? && current_user.id == 1
  end

  def current_user
    @current_user ||= User.first(id: session[:user_id])
  end

  def reel_owner_check reel_id
    login_check
    unless current_user.highlight_reels.map{|reel| reel[:id]}.include? reel_id.to_i
      redirect "/"
    end
  end
end
