require 'sinatra'
Dir.entries(File.dirname(__FILE__)).each do |f|
  next if f.to_s.include? "#"
  require_relative f[0..-4] if f.include? 'controller'
end
