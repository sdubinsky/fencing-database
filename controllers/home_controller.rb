require_relative 'base_controller'

class HomeController < BaseController
  get '/?' do
    @score_strip_locations = [:all, :fotl_warning_box, :fotl_half, :middle, :fotr_half, :fotr_warning_box]
    @tournaments = Tournament.order_by(:tournament_name)
    if !params&.empty?
      params.transform_values!{|v| v.strip}
      @fencers = []
      params['page'] = 1 unless params['page']
      if params['submit-search'] == "Search Fencers"
        @gfycats = []
        @fencers = Fencer.search_with_params params
      elsif params['submit-search'] == 'Random Clip'
        @gfycats = Helpers.get_touches_query_gfycats(DB, params).order_by(Sequel.lit("random()")).limit(1).all
        redirect "/clip/" + @gfycats[0].gfycat_gfy_id
      else
        @fencers = []
        @gfycats = Helpers.get_touches_query_gfycats(DB, params)
        @get_string = params.map do |k, v|
          if k == 'page'
            next
          end
          "#{k}=#{v}"
        end.compact.join "&"
      end
      erb :index
    else
      @gfycats = Gfycat.order_by(Sequel.lit("random()")).limit(10)
      @fencers = []
      erb :index
    end
  end

  post '/' do
    @tournaments = Tournament.order_by(:tournament_name)
    if params['submit-search'] == 'Search Fencers'
      @gfycats = []
      @fencers = Fencer.search_with_params params
    else
      @fencers = []
      params['page'] = 1 unless params['page']
      @gfycats = Helpers.get_touches_query_gfycats(DB, params).map{|gfy| gfy[:gfycat_gfy_id]}
      @get_string = params.map do |k, v|
        if k == 'page'
          next
        end
        "#{k}=#{v}"
      end.compact.join "&"
    end
    @score_strip_locations = [:all, :fotl_warning_box, :fotl_half, :middle, :fotr_half, :fotr_warning_box]

    erb :index
  end

  get '/search/?' do
    redirect '/'
  end

  get '/stats/?' do
    @tournaments = Tournament.order_by(:tournament_year).all

    @genders = ['male', 'female']
    @total = FormResponse.total tournament: params['tournament-filter'], fencer_name: params['fencer-filter'], gender: params['gender-filter'], weapon: params['weapon-filter']
    @location = FormResponse.most_popular_location tournament: params['tournament-filter']
    @most_popular_location = @location[:strip_location]
    @most_popular_location = @most_popular_location.gsub("fotr", "FOTR").gsub("fotl", "FOTL").gsub("_", " ")
    @most_hit_location = FormResponse.most_popular_hit tournament: params['tournament-filter']
    @most_popular_hit = @most_hit_location[:body_location]
    @most_popular_hit = @most_popular_hit.gsub("_", " ") or ""

    if logged_in?
      @current_user_responses = current_user.form_responses.count
    end

    query = FormResponse.build_query tournament: params['tournament-filter'], fencer_name: params['fencer-filter'], weapon: params['weapon-filter']
    @color_map = FormResponse.heatmap_colors query
    @fencer_names = ['all'] + Gfycat.fencer_names
    erb :stats
  end

  post '/error_report' do
    body = JSON.parse(@request.body.read)
    gfy_id = body['gfy_id']
    ErrorReport.create(
      gfycat_gfy_id: gfy_id,
      created_date: Time.now.to_i
    )
  end

  get '/signup/?' do
    erb :signup
  end

  post '/signup/?' do
    if not params['signup-username'] and params['signup-password']
      @error_message = "Please add a username and password"
      erb :signup
    elsif params['spam-test'] != 'fencing'
      sleep 2
      @error_message = 'Please put "fencing" in the last field""'
      erb :signup
    else
      begin
        user = User.create(
          username: params['signup-username'],
          password_hash: BCrypt::Password.create(params['signup-password']),
          email: params['signup-email'],
          created_date: Time.now.to_i
        )
      rescue Sequel::UniqueConstraintViolation
        @error_message = "username already taken."
        erb :signup
      else
        session[:user_id] = user.id
        redirect '/'
      end
    end
  end

  get '/login/?' do
    erb :login
  end

  get '/logout/?' do
    session.delete :user_id
    redirect '/'
  end

  post '/login/?' do
    username = params['login-username']
    user = User.first(username: username)
    if not user
      @error_message = "Error: invalid username or password"
      return erb :login
    end
    begin
      password = BCrypt::Password.new user.password_hash
    rescue BCrypt::Errors::InvalidHash
      @error_message = "Error: Invalid password"
      logger.info "failed to log in user #{params['username']}"
      return erb :login
    end

    if password == params['login-password']
      session[:user_id] = user.id
    end
    redirect params['url']  if params['url']
    redirect '/'
  end

  get '/docs/?' do
    erb :docs
  end

  get '/jbkdflks/?' do
    redirect 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
  end
end
