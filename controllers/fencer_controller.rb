require_relative 'base_controller'

class FencerController < BaseController
  get '/:fie_id1/:fie_id2/?' do
    @fencer1 = Fencer.first fie_id: params[:fie_id1]
    @fencer2 = Fencer.first fie_id: params[:fie_id2]

    query = Gfycat.where(left_fencer_id: @fencer1.id, right_fencer_id: @fencer2.id).or(left_fencer_id: @fencer2.id, right_fencer_id: @fencer1.id)

    @bouts = Bout.head_to_head_bouts @fencer1, @fencer2
    @fencer1_score = @bouts.map{|b| b.final_touch.score_for_fencer @fencer1.id}.reduce(:+)
    @fencer2_score = @bouts.map{|b| b.final_touch.score_for_fencer @fencer2.id}.reduce(:+)
    erb :head_to_head
  end

  get '/:fie_id' do
    @fencer = Fencer.first(fie_id: params['fie_id'])
    query = Gfycat.join(:form_responses, stats_id: :gfycat_gfy_id).where(Sequel.|(left_fencer_id: @fencer.id, right_fencer_id: @fencer.id))
    @left_touches = Gfycat.where(left_fencer_id: @fencer.id, touch: 'left', status: 'valid').exclude(right_fencer_id: nil, status: 'valid')
    @right_touches = Gfycat.where(right_fencer_id: @fencer.id, touch: 'right', status: 'valid').exclude(left_fencer_id: nil, status: 'valid')

    left_received = Gfycat.where(left_fencer_id: @fencer.id, touch: 'right', status: 'valid').exclude(right_fencer_id: nil, status: 'valid')
    right_received = Gfycat.where(right_fencer_id: @fencer.id, touch: 'left', status: 'valid').exclude(left_fencer_id: nil, status: 'valid')

    received_by_opponent = left_received.select(:right_fencer_id).union(right_received.select(:left_fencer_id), all: true).group_and_count(:right_fencer_id)
    form_responses = FormResponse.where(stats_id: @left_touches.select(:gfycat_gfy_id)).or(stats_id: @right_touches.select(:gfycat_gfy_id))

    @touches_by_opponent = @left_touches.select(:right_fencer_id).union(@right_touches.select(:left_fencer_id), all: true).group_and_count(:right_fencer_id)

    @fencers = Bout.select(:left_fencer_id).where(right_fencer_id: @fencer.id).union(Bout.select(:right_fencer_id).where(left_fencer_id: @fencer.id), all: true).group_and_count(:left_fencer_id).reverse.from_self(alias: :opponents)

    @fencers = @fencers.select(Sequel[:received][:count].as(:touches_received), Sequel[:touches][:count].as(:touches_scored), :left_fencer_id, Sequel[:opponents][:count].as(:bouts)).join(@touches_by_opponent.as(:touches), right_fencer_id: :left_fencer_id).join(received_by_opponent.as(:received), Sequel[:received][:right_fencer_id] => Sequel[:touches][:right_fencer_id]).from_self

    @fencers = @fencers.select(:last_name, :first_name, :fie_id, :touches_scored, :touches_received, :bouts, :nationality).join(DB[:fencers], id: :left_fencer_id).order(:bouts)
    @partial_title = "opponents"
    @location = form_responses.group_and_count(:body_location).reverse(:count).first
    @location = @location&.body_location ? @location.body_location : 'unknown'
    @color_map = FormResponse.heatmap_colors query
    erb :fencer
  end
end
