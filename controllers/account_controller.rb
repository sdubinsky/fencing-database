require 'bcrypt'
require_relative 'base_controller'

class AccountController < BaseController
  get '/' do
    login_check

    @submissions = FantasySubmission.where(user_id: current_user.id).where(tournament_id: Tournament.select(:id).where(tournament_year: '2024/2025')).reverse(:created_at)
    erb :account
  end

  get '/reset_password' do
    login_check
    erb :reset_password
  end

  post '/reset_password' do
    login_check
    if params['reset-password-password'] != params['reset-password-password-two']
      @error_message = "Sorry, the passwords didn't match"
      return erb :reset_password
    end
    new_password = BCrypt::Password.create(params['reset-password-password'])
    current_user.update(password_hash: new_password)
    redirect '/account'
  end
end
