require_relative 'base_controller'

class ReelController < BaseController
  get '/?' do
    login_check
    @in_progress_reels = current_user.highlight_reels.select{|a| !a.completed}
    @completed_reels = current_user.highlight_reels.select{|a| a.completed}
    erb :reels
  end

  get '/new/?' do
    login_check
    @fencers = Fencer.select(:id, Sequel.lit("(last_name || ' ' || first_name) as full_name")).order_by(:full_name)
    @nationalities = Fencer.select(:nationality).distinct.order_by(:nationality).all.map{|a| a.nationality}
    @tournaments = Tournament.order_by(:tournament_name)
    @score_strip_locations = [:all, :fotl_warning_box, :fotl_half, :middle, :fotr_half, :fotr_warning_box]

    erb :new_reel
  end

  post '/create' do
    logger.info "creating new reel for user #{@current_user&.username}"
    login_check
    params['page'] = -1
    gfycats = Helpers.get_touches_query_gfycats DB, params
    reel = HighlightReel.create(
      author: params['author'],
      title: params['title'],
      last_name: params['lastname'],
      first_name: params['firstname'],
      filter_params: params.to_json,
      tournament: params['tournament'],
      user_id: current_user.id
    )
    reel.save
    params['page'] = -1
    DB.transaction do
      gfycats.each do |gfy|
        ReelClip.create(
          gfycat_gfy_id: gfy[:gfycat_gfy_id],
          highlight_reel: reel,
        )
      end
    end
    redirect "/reels/#{reel.id}"
  end


  get '/help/?' do
    erb :reel_help
  end

  get '/:id/?' do
    reel_owner_check params['id']
    @reel = HighlightReel[params['id']]
    @clip_count = ReelClip.where(selected: true, highlight_reel_id: @reel.id).count
    @seconds = @clip_count * 10
    @hours = @seconds / 3600
    @seconds = @seconds % 3600
    @minutes = @seconds / 60
    @seconds = @seconds % 60
    @unsorted_clip_count = ReelClip.where(selected: nil, highlight_reel_id: @reel.id).count
    erb :reel
  end

  get '/:id/judge/?' do
    reel_owner_check params['id']
    clips = ReelClip.join(:gfycats, gfycat_gfy_id: :gfycat_gfy_id).where(selected: nil, highlight_reel_id: params['id']).order_by(Sequel.lit('random()')).limit(2).all
    if params[:gfy]
      puts params[:gfy]
      @clip = Gfycat.first(gfycat_gfy_id: params[:gfy])
      @next_clip = clips[0]
    else
      @clip = clips[0]
      @next_clip = clips[1]
    end
    @clip_count = ReelClip.where(selected: true, highlight_reel_id: params['id']).count
    @unsorted_clip_count = ReelClip.where(selected: nil, highlight_reel_id: params['id']).count
    unless @clip
      redirect to("/#{params['id']}")
    end
    erb :reel_clip
  end

  get '/:id/export.sh/?' do

    reel_owner_check params['id']
    @reel = HighlightReel[params['id']]
    @reel.update(completed: true)
    [200, {'Content-Type' => 'text/plain'}, @reel.export_reel]
  end

  get '/:id/upload/?' do
    reel_owner_check params['id']
    @reel = HighlightReel[params['id']]
    @reel.update(completed: true, ready_for_upload: true)
    redirect '/reels'
  end

  get '/:id/newround' do
    reel_owner_check params['id']
    @reel = HighlightReel[params['id']]
    DB.transaction do
      ReelClip.where(selected: nil, highlight_reel_id: params['id']).each do |clip|
        clip.selected = false
        clip.save
      end
    end
    DB.transaction do
      ReelClip.where(selected: true, highlight_reel_id: params['id']).each do |clip|
        clip.round = @reel.round
        clip.selected = nil
        clip.save
      end
      @reel.round += 1
      @reel.save
    end
    redirect "/reels/#{params['id']}/"
  end

  post '/submit/?' do
    body = JSON.parse(@request.body.read)
    reel_owner_check body['reelId']
    @clip = ReelClip.first(selected: nil, highlight_reel_id: body['reelId'], gfycat_gfy_id: body['clipId'])

    case body['result']
    when 'accept'
      @clip.selected = true
    when 'reject'
      @clip.selected = false
    end
    @clip.save
  end

end
