require_relative 'base_controller'

class AdminController < BaseController
  get '/error_report/?' do
    admin_check

    @bad_gfy = Gfycat.where(gfycat_gfy_id: ErrorReport.select(:gfycat_gfy_id).where(resolved: false).limit(1)).first
    redirect to('../') unless @bad_gfy

    erb :error_report
  end

  post '/error_report/?' do
    admin_check

    request.body.rewind
    gfy = Gfycat.where(gfycat_gfy_id: params['gfycat_gfy_id']).first
    error_reports = ErrorReport.where(gfycat_gfy_id: params['gfycat_gfy_id'])
    logger.info "Fixing gfy #{gfy.gfycat_gfy_id}"
    params.each_pair do |k, v|
      next if k == 'validity'
      next if k == 'gfycat_gfy_id'
      next if v.empty?
      k = k.gsub('-', "_")
      v = v.to_i if k.include? 'score'
      logger.info "changing #{k} to #{v}"
      gfy.send("#{k}=", v)
    end

    gfy.normalize_names(true)
    gfy.status = params['validity']

    gfy.save

    error_reports.each do |report|
      report.resolved = true
      report.save
    end

    redirect to('/error_report')
  end

  get '/fix_gfy/?' do
    admin_check
    @bad_gfy = Gfycat.where{
      Sequel.&({left_fencer_id: nil}, {status: nil})
    }.or{
      Sequel.&({right_fencer_id: nil}, {status: nil})
    }.order_by(Sequel.lit("RANDOM()")).limit(1).first
    @left_fencers = Fencer.find_name_possibilities(@bad_gfy.fotl_name, @bad_gfy.tournament.id)
    @right_fencers = Fencer.find_name_possibilities(@bad_gfy.fotr_name, @bad_gfy.tournament.id)
    erb :fix_gfy
  end

  post '/fix_gfy/?' do
    admin_check
    request.body.rewind
    gfy = Gfycat.where(gfycat_gfy_id: params['gfycat_gfy_id']).first
    if params['fotl_name']
      gfy.fotl_name = params['fotl_name']
    end
    if params['fotr_name']
      gfy.fotr_name = params['fotr_name']
    end
    gfy.normalize_names true
    redirect to('/fix_gfy')
  end
end
