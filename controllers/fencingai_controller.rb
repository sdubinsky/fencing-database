require_relative 'base_controller'

class FencingAiController < BaseController
  post '/submit/?' do
    body = JSON.parse(@request.body.read)
    logger.info "got fencing ai result: " + body['keycode']
    FencingAiResult.create(
      keycode: body['keycode'],
      timestamp: Time.new.to_i,
      gfycat_gfy_id: body['clipId'],
    )
  end

  get '/error/?' do
    "Oops"
  end

  get '/clip/?' do
    @clip = FencingAiClip.order_by(Sequel.function(:random)).first
    @keycodes = FencingAiKeycodes.select(:keycode, :meaning).map{|code| [code.keycode, code.meaning]}.to_h
    erb :fencing_ai_clip
  end

  get '/stats/?' do
    @results = FencingAiResult.results
    erb :fencing_ai_stats
  end

end
