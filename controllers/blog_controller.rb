require_relative 'base_controller'

class BlogController < BaseController

  get '/' do
    erb 'blog/index'.to_sym
  end

  get '/:title/?' do
    erb "blog/#{params['title']}".to_sym
  end
end
