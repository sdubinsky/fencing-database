require_relative 'base_controller'
require 'json'

class ClipController < BaseController

  get '/test/?' do
    headers "Content-Type" => "text/html"
    erb :test
  end

  get '/:gfycat_gfy_id/?' do
    @score_strip_locations = [:fotl_warning_box, :fotl_half, :middle, :fotr_half, :fotr_warning_box]
    @score_body_locations = [:hand, :front_arm, :torso, :head, :front_leg, :foot, :back_arm, :back_leg]
    begin
      @gfycat = Gfycat.first(Sequel.ilike(:gfycat_gfy_id, params['gfycat_gfy_id']))
      redirect "/clip/" unless @gfycat
      logger.info "Showing #{@gfycat.gfycat_gfy_id}"
    rescue RuntimeError
      return "Please seed the DB by sending a GET request to /update_gfycat_list"
    end
    @questions = FormResponse.questions_to_html(FormResponse.questions(@gfycat.weapon))
    erb :clip
  end

  get '/?' do
    @gfycat = Gfycat.random_gfycat_id params['weapon'], params['tournament']
    redirect "/clip/#{@gfycat.gfycat_gfy_id}"
  end

  post '/submit/?' do
    response = FormResponse.create(
      initiated: params['initiated-action'],
      strip_location: params['strip-location'],
      body_location: params['score-body-select'],
      additional_questions: params.to_json,
      stats_id: params['gfycat-id'],
      created_date: Time.now.to_i,
      user_id: session[:user_id]
    )

    logger.info("new submission: #{response.to_s}")
    redirect "/clip/=#{params['gfycat-id']}"
  end
end
