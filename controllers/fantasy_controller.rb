require 'debug'
require_relative 'base_controller'

class FantasyController < BaseController
  get '/help/?' do
    erb :fantasy_help
  end


  get '/:tournament_id/?:gender?/?' do
    tournament_id = params[:tournament_id]
    @tournament = Tournament.first tournament_id: tournament_id
    if !@tournament || @tournament.start_time > Time.now.utc.to_i
      redirect '/fantasy' unless current_user
    end

    @gender = params[:gender] || @tournament.gender
    redirect '/fantasy' unless @gender
    @tournament_title = "#{@tournament.tournament_name} (#{@gender} #{@tournament.weapon})"
    if @tournament.start_time < Time.now.to_i
      @supporters = ['TeaPoweredRobot', 'lightbxlb', 'fanxan', 'ajslim', 'trueflipmode', 'fritzdeath', 'angothoron']
      @top_seeds = Fencer.join(
        FantasySubmission.where(Sequel[:fantasy_submissions][:tournament_id] => @tournament.id, Sequel[:fantasy_submissions][:gender] => @gender).
          join(:fencers, id: :entry_one).
          group_and_count(Sequel[:fencers][:id]).as(:a), Sequel[:fencers][:id] => Sequel[:a][:id]).
                     join(:fencers_tournaments, tournament_id: @tournament.id, fencer_id: Sequel[:a][:id]).
                     reverse(:count)

      @middle_seeds = Fencer.join(
        FantasySubmission.where(tournament_id: @tournament.id, Sequel[:fantasy_submissions][:gender] => @gender).
          join(:fencers, id: :entry_eleven).
          group_and_count(Sequel[:fencers][:id]).as(:a), Sequel[:fencers][:id] => Sequel[:a][:id]).
                        join(:fencers_tournaments, tournament_id: @tournament.id, fencer_id: Sequel[:a][:id]).
                        reverse(:count)
      @bottom_seeds = Fencer.join(
        FantasySubmission.where(tournament_id: @tournament.id, Sequel[:fantasy_submissions][:gender] => @gender).
          join(:fencers, id: :entry_twenty_one).
          group_and_count(Sequel[:fencers][:id]).as(:a), Sequel[:fencers][:id] => Sequel[:a][:id]).
                        join(:fencers_tournaments, tournament_id: @tournament.id, fencer_id: Sequel[:a][:id]).
                        reverse(:count)
      @entries = FantasySubmission.where(tournament_id: @tournament.id, gender: @gender).where{score > 0}.join(:users, id: :user_id).reverse(:score)
      return erb :fantasy_results
    end

    if @submission = FantasySubmission.first(user_id: current_user.id, tournament_id: @tournament.id, gender: @gender)
      logger.warn "#{current_user.id} tried to submit twice"
      return erb :already_entered
    end

    @entries = FencersTournament.where(tournament_id: @tournament.id, gender: @gender).join(:fencers, id: :fencer_id).order_by(:seed, :fencer_id).all
    @stats = {}
    @entries.each do |entry|
      @stats[entry[:fie_id]] = Fencer[entry[:fencer_id]].fantasy_stats
    end
    erb :fantasy_submission
  end

  get '/' do
    if params[:season]
      @season = params[:season] + '/' + (params[:season].to_i + 1).to_s
    else
      @season = '2024/2025'
    end
    @supporters = ['TeaPoweredRobot', 'lightbxlb', 'fanxan', 'ajslim', 'fritzdeath', 'trueflipmode']
    @overall_top_ten = User.join(FantasySubmission.select_group(:user_id).join(Tournament.dataset, id: :tournament_id).where(tournament_year: @season).select_append{sum(score).as(total)}, user_id: :id).exclude(total: nil).reverse(:total, :username).limit(10)

    @epee_top_ten = User.join(FantasySubmission.select_group(:user_id).join(Tournament.dataset, id: :tournament_id).where(tournament_year: @season, weapon: 'epee').select_append{sum(score).as(total)}, user_id: :id).exclude(total: nil).reverse(:total, :username).limit(10)
    @sabre_top_ten = User.join(FantasySubmission.select_group(:user_id).join(Tournament.dataset, id: :tournament_id).where(tournament_year: @season, weapon: 'sabre').select_append{sum(score).as(total)}, user_id: :id).exclude(total: nil).reverse(:total, :username).limit(10)
    @foil_top_ten = User.join(FantasySubmission.select_group(:user_id).join(Tournament.dataset, id: :tournament_id).where(tournament_year: @season, weapon: 'foil').select_append{sum(score).as(total)}, user_id: :id).exclude(total: nil).reverse(:total, :username).limit(10)
    @single_top_ten = FantasySubmission.exclude(score: nil).join(Tournament.dataset, id: :tournament_id).where(tournament_year: @season).join(User.dataset, :id => Sequel[:fantasy_submissions][:user_id]).reverse(:score).select(:username, :tournament_name, :score).limit(10).all
    @single_rarity_top_ten = FantasySubmission.exclude(score: nil).join(Tournament.dataset, id: :tournament_id).where(tournament_year: @season).join(User.dataset, :id => Sequel[:fantasy_submissions][:user_id]).reverse(:rarity_weighted_score).select(:username, :tournament_name, :rarity_weighted_score).limit(10).all
    @rarity_weighted_top_ten = User.join(FantasySubmission.select_group(:user_id).join(Tournament.dataset, id: :tournament_id).where(tournament_year: @season).select_append{sum(rarity_weighted_score).as(total)}, user_id: :id).exclude(total: nil).reverse(:total, :username).limit(10)
    @upcoming_tournaments = Tournament.where{ start_time > Time.now.to_i}.where{start_time < (Time.now.to_i + 60*60*24*8)}.where(tournament_year: @season).reverse(:start_time).all.select{|a| a.fencers.count > 20}
    @previous_tournaments = Tournament.where { start_time < Time.now.to_i }.where(tournament_year: @season).reverse(:start_time)
    erb :fantasy
  end

  post '/submit/?' do
    login_check
    entry_one = Fencer.select(:id).first(fie_id: params['entries-one'])
    entry_eleven = params['entries-eleven'] ? Fencer.select(:id).first(fie_id: params['entries-eleven']) : nil
    entry_twenty_one = params['entries-twenty-one'] ? Fencer.select(:id).first(fie_id: params['entries-twenty-one']) : nil
    tournament = Tournament[params['tournament-id'].to_i]
    gender = params['gender']
    if tournament.start_time < Time.now.to_i
      logger.warn "#{current_user.id} tried to access an already-started tournament"
      redirect "/fantasy/#{tournament.tournament_id}/#{gender}"
    end
    if FantasySubmission.where(user_id: current_user.id, tournament_id: tournament.id, gender: gender).count > 0
      logger.warn "#{current_user.id} tried to submit twice"
      redirect '/fantasy'
    end

    FantasySubmission.create(
      user_id: current_user.id,
      created_at: Time.now.to_i,
      entry_one: entry_one.id,
      entry_eleven: entry_eleven&.id,
      entry_twenty_one: entry_twenty_one&.id,
      tournament_id: tournament.id,
      gender: gender
    )
    redirect '/fantasy'
  end
end
