require 'sinatra'
require 'logger'
require './helpers'

class BaseController < Sinatra::Application
  set :root, File.dirname(__FILE__) + '/../'

  set :allow_origin, '*'
  set :allow_methods, 'GET,HEAD,POST'
  set :allow_headers, "content-type,if-modified-since"
  set :expose_headers, "location,link"
  set :allow_credentials, true

  configure :development do
    set :session_secret, File.read("#{root}/session_secret.txt")
    set :sessions, same_site: :lax
    logger = Logger.new($stdout)
    logger.level = :info
    set :logger, logger
  end

  configure :production do
    set :session_secret, ENV['SESSION_SECRET']
    use Rack::Session::Cookie, :key => 'rack.session',
        :path => '/',
        :secret => session_secret

  end

  before do
    body = request.body.read
    View.create(
      endpoint: env['REQUEST_PATH'],
      http_method: env['REQUEST_METHOD'],
      form_data: body.empty? ? params.to_json : body,
      country_code: env['HTTP_CF_IPCOUNTRY'],
      created_at: Time.now.to_i
    )
    @request.body.rewind
  end

  include Helpers
end
