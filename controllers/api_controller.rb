require_relative 'base_controller'

class ApiController < BaseController

  before do
    content_type 'application/json'
  end

  not_found do
    status 404
    return ''
  end

  get '/help/?' do
    content_type 'text/html'
    erb :help
  end

  get '/bouts/?:id_number?' do
    if params["id_number"]
      bout = Bout[params["id_number"].to_i]
      return bout.to_json if bout
      status 404
      return "bout not found"
    else
      Bout.json
    end
  end

  get '/tournaments/?:name?' do
    if params["name"]
      tournament = Tournament.first(tournament_id: params["name"])
      return tournament.to_json if tournament
      status 404
      return "tournament not found"
    else
      Tournament.json
    end
  end

  get '/fencers/?:fie_id' do
    fencer = Fencer.first(fie_id: params['fie_id'])
    return fencer.to_json if fencer
    status 404
    return "fencer not found"
  end

  get '/clips/questions/?:weapon?/?' do
    unless params['weapon']
      status 400
      {error_message: "Error: Please specify a weapon"}.to_json
    else
      FormResponse.questions(params['weapon']).to_json
    end
  end

  get '/clips/random/?:weapon?/?' do
    gfy = Gfycat.random_gfycat_id params['weapon']
    return gfy.to_json
  end

  get '/clips/answers/?:offset?/?' do
    if params['offset']
      offset = params['offset'].to_i
    else
      offset = 0
    end
    response = FormResponse.order(:created_date).limit(100, offset)
    count = response.count
    return {
      count: count,
      offset: offset,
      answers: response.map{|x| x.as_dict}
    }.to_json
  end

  get '/clips/:gfycat_gfy_id/?' do
    if params["gfycat_gfy_id"]
      gfy = Gfycat.first Sequel.ilike(:gfycat_gfy_id, params["gfycat_gfy_id"])
      return gfy.to_json if gfy && gfy.valid?
      status 404
      return "gfycat not found"
    end
  end

  get '/clips/:gfycat_gfy_id/answers/?' do
    response = FormResponse.where Sequel.ilike(:stats_id, params["gfycat_gfy_id"])
    return response.map{|x| x.as_dict}.to_json if response
    status 404
    return "response not found"
  end

  post '/clips/submit/?' do
    #To generate an API key:
    # key = SecureRandom.base64(16) # send this to the user
    # key_hash = Digest::SHA2.new << user.password_hash
    api_key = env['HTTP_X_AUTHENTICATION_HEADER']
    key_hash = Digest::SHA2.new << api_key
    api_key = ApiKey.first(key: key_hash.to_s)

    if not api_key
      status 401
      return
    end
    request.body.rewind
    body = JSON.parse request.body.read
    user = api_key.user


    if not body['gfycat-gfy-id']
      status 400
      return {"error_message" => "Error: Missing gfycat id"}.to_json
    end
    begin
      response = FormResponse.create(
        initiated: body['initiated-action']&.downcase,
        strip_location: body['strip-location']&.downcase,
        body_location: body['score-body-select']&.downcase,
        stats_id: body['gfycat-gfy-id'],
        created_date: Time.now.to_i,
        user_id: user.id
      )
    rescue => e
      status 400
      puts "error: #{e.message}"
      return {error: 'parse error, please try again or contact support'}.to_json
    end
    logger.info("new submission: #{response.to_s}")
    status 204
  end
end
