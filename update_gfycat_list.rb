require 'sequel'
require 'psych'
require 'aws-sdk-s3'

prefixes = ['paris']

if ENV['DATABASE_URL']
  connstr = ENV['DATABASE_URL']
else
  config = Psych.load_file("./config.yml")
  db_config = config['database']
  if db_config['db_username'] or db_config['db_password']
    login = "#{db_config['db_username']}:#{db_config['db_password']}@"
  else
    login = ''
  end
  connstr = "postgres://#{login}#{db_config['db_address']}/#{db_config['db_name']}"
end
DB = Sequel.connect connstr
require './models/init'

def log_time
  Time.now.strftime("%H:%M:%S")
end

secrets = Psych.load_file('r2_secrets.yml')
url = 'https://4dd32318115734d076dd61a3ace4f5ec.r2.cloudflarestorage.com'
client = Aws::S3::Client.new(access_key_id: secrets['access_key'], secret_access_key: secrets['secret_key'], region: 'auto', endpoint: url)

old_gfycats = Gfycat.select(:gfycat_gfy_id).all.sort{|a, b| a.gfycat_gfy_id <=> b.gfycat_gfy_id}
tournaments = Tournament.select(:tournament_id).to_a
$stderr.puts "old gfycat count: #{old_gfycats.length}"

def pull_gfycats client, prefix
  puts prefix
  tries = 3
  next_round = client.list_objects_v2({bucket: 'fencingdatabase', prefix: prefix})
  cursor = next_round[:next_continuation_token]
  all_gfys = next_round[:contents]
  loop do
    begin
      next_round = client.list_objects_v2({bucket: 'fencingdatabase', prefix: prefix, continuation_token: cursor})
      cursor = next_round[:next_continuation_token]
      all_gfys = all_gfys + next_round[:contents]
      break if next_round[:contents].length < 1000
    rescue => e
      puts e.message
      tries -= 1
      break if tries == 0
    end
  end
  all_gfys
end

all_gfycats = []
prefixes.each do |prefix|
  all_gfycats.append(pull_gfycats(client, prefix).sort{|a, b| a[:key] <=> b[:key]}.uniq)
end
#File.open('gfys.txt', 'w') do |f|
#   all_gfycats.each{|a| f.puts a[:key]}
# end
$stderr.puts "finished getting gfy list and started filtering at " + log_time

new_gfys = all_gfycats.flatten.reject{|a| old_gfycats.bsearch{|b| a[:key].split('/')[1][0...-4] <=> b.gfycat_gfy_id }}
new_gfycats = Queue.new(new_gfys)
results = Queue.new

$stderr.puts "finished filtering at " + log_time
$stderr.puts "new gfycats count: #{new_gfycats.length}"

DB = Sequel.connect(connstr, max_connections: 12)
require './models/init'

threads = 0.upto(12).map do
  Thread.new do
    until new_gfycats.empty?
      gfy = new_gfycats.pop(non_block=true)
      key = gfy[:key].split("/")[1][0...-4]
      resp = client.get_object({bucket: 'fencingdatabase', key: gfy[:key], range: "bytes=0-1"})

      tags = resp[:metadata]
      if tags['leftscore'] && tags['leftscore'].length > 0
        left_score = tags['leftscore'].to_i
      else
        left_score = -1
      end

      if tags['rightscore'] && tags['rightscore'].length > 0
        right_score = tags['rightscore'].to_i
      else
        right_score = -1
      end

      if tournaments.include? tags['tournament'] and not tags['tournament'].nil?
        $stderr.puts "#{tags['tournament']} doesn't exist"
        exit(1)
      end
      tournament_id = tags['tournament']
      begin
        results.push([
          key,
          tournament_id,
          tags['weapon'],
          tags['gender'],
          Time.now.to_i,
          tags['leftname'],
          tags['rightname'],
          left_score,
          right_score,
          tags['touch'],
          tags['time'],
          tags['event_type']
        ])
      rescue => e
        $stderr.puts e.to_s
        exit 1
      end
    end
  end
end

threads.each {|t| t.join}
arr = []
until results.empty?
  arr.push results.pop
end
arr = arr.compact
DB[:gfycats].import([:gfycat_gfy_id, :tournament_id, :weapon, :gender, :created_date, :fotl_name, :fotr_name, :left_score, :right_score, :touch, :scored_time, :event_type], arr)
$stderr.puts "done at " + log_time
