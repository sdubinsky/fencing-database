require 'json'
require 'sequel'
require 'excon'
require 'psych'

module Helpers
  def confidence_interval wins, losses, confidence_level
    sample_size = wins + losses * 1.0
    win_pct = wins / sample_size
    loss_pct = losses / sample_size
    z = 1.65 if confidence_level == 90
    z = 1.96 if confidence_level == 95
    z = 2.58 if confidence_level == 99
    error_bound = z * (Math.sqrt(win_pct * loss_pct / sample_size))

    [(win_pct + error_bound).truncate(2), (win_pct - error_bound).truncate(2)]
  end

  def download_entries url
    connection = Excon.new url
    page = connection.get
    page = page.body
    lines = page.gsub("\n", "")
    lines = lines.split("<tr>")
    lines = lines.select{|l| l.include? "<td"}
    lines = lines.map{|row| row.split("</td>").map{|entry| entry.gsub(/<.?td.*?>/, "").strip}}
  end

  def db_address
  if ENV["DATABASE_URL"]
    ENV["DATABASE_URL"]
  else
    config = Psych.load_file("./config.yml")
    db_config = config['database']
    if db_config['db_username'] or db_config['db_password']
      login = "#{db_config['db_username']}:#{db_config['db_password']}@"
    else
      login = ''
    end
    "postgres://#{login}#{db_config['db_address']}/#{db_config['db_name']}"
  end
end

  def country_code country
    special_codes = {
      'japan' => 'jpn',
      'united states' => 'usa',
      'great britain' => 'gbr',
      'switzerland' => 'sui',
      'iraq' => 'irq',
      'lebanon' => 'lbn',
      'china' => 'chn',
      'austria' => 'aut',
      'el salvador' => 'slv',
      'ireland' => 'irl',
      'puerto rico' => 'pri',
      'türkiye' => 'tur'
    }

    code = special_codes[country.strip.downcase]
    code ||= country[0...3]
    code.upcase
  end
end
