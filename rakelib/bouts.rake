require 'json'
require 'excon'
require 'psych'
require 'sequel'

namespace :bouts do
  desc 'show winner of a bout'
  task :winner, [:bout_id] do |t, args|
    bout_id = args[:bout_id].to_i
    require 'sequel'
    Sequel.connect db_address do |db|
      require './models/init'
      top_gfycat = Gfycat.where(bout_id: bout_id).
                     where(status: 'valid').
                     select(:gfycat_gfy_id, :left_score, :right_score,
                            :left_fencer_id, :right_fencer_id,
                            Sequel.+(:left_score, :right_score).as(:total)).
                     order(:total).
                     reverse.first
      winner = top_gfycat.left_score > top_gfycat.right_score ? top_gfycat.left_fencer : top_gfycat.right_fencer
      puts "the winner of bout #{bout_id} is #{winner.name}"
    end
  end

  desc 'show bout summaries'
  task :summary, [:bout_id] do |t, args|
    require 'sequel'
    Sequel.connect db_address do |db|
      require './models/init'
      if args[:bout_id]
        bouts = [Bout.first[args[:bout_id].to_i]]
      else
        bouts = Bout
      end

      bouts.each{ |bout| puts bout.to_s if bout.winner }
    end
  end

  desc 'get bout differential'
  task :differential, [:weapon] do |t, args|
    # the default for the hash is an array of two elements
    # the left element is the times the eventual winner was leading with this differential
    # the right element is the times the eventual loser was leading with this differential
    differentials = Hash.new {|hsh, key| hsh[key] = Array.new([0, 0])}
    require 'sequel'
    Sequel.connect db_address do |db|
      require './models/init'
      Bout.exclude(winner: nil).each do |bout|
        if args[:weapon]
          next unless bout.gfycats[0][:weapon ] == args[:weapon]
        end
        #for each touch in every bout
        bout.gfycats.each do |t|
          #get the difference between the scores
          differential = (t[:left_score] - t[:right_score]).abs
          if differential > 15
            next
          end
          #skip it if they're tied
          next if differential == 0
          # if the left fencer is leading
          if t[:left_score] > t[:right_score]
            #if they're the eventual winner
            if t[:left_fencer_id] == bout.winner
              differentials[differential][0] += 1
            else
              differentials[differential][1] += 1
            end
          elsif t[:right_score] > t[:left_score]
            if t[:right_fencer_id] == bout.winner
              differentials[differential][0] += 1
            else
              differentials[differential][1] += 1
            end
          end
        end
      end

      differentials.sort_by{|a, b| a}.each do |k, v|
        interval_90 = confidence_interval v[0], v[1], 90
        interval_95 = confidence_interval v[0], v[1], 95
        interval_99 = confidence_interval v[0], v[1], 99
        puts "Lead size: #{k}: winner: #{v[0]} loser: #{v[1]}.  total touches: #{v[0] + v[1]}.  Percentage: #{(v[0].to_f / (v[0] + v[1])).truncate(2)}\n\t 90% confidence interval: #{interval_90}\n\t95% confidence interval: #{interval_95}\n\t99% confidence interval: #{interval_99}"
      end
    end
  end

  desc 'comebacks'
  task :comebacks, [:weapon] do |t, args|
    require 'sequel'
    comebacks = Hash.new {|hsh, key| hsh[key] = Array.new}
    Sequel.connect db_address do |db|
      require './models/init'
      Bout.each do |bout|
        if args[:weapon]
          next unless bout.gfycats[0][:weapon ] == args[:weapon]
        end
        curr = bout.gfycats[0]
        curr_diff = 0
        differential = 0
        # for each touch in every bout
        bout.gfycats.each do |t|
          differential = (t[:left_score] - t[:right_score]).abs
          next if differential > 9 # skip big diffs
          # if the differential is bigger than the current one,
          # and the eventual winner is losing
          if t.left_score < t.right_score && bout.winner == t.left_fencer_id
            curr = t
            curr_diff = differential
          elsif t.right_score < t.left_score && bout.winner == t.right_fencer_id
            curr = t
            curr_diff = differential
          end
        end
        comebacks[curr_diff] << curr
      end
    end
    comebacks.sort_by{|a, b| a}.each do |k, v|
      puts "comeback starting at #{k} (happened #{v.count} times):"
      v.each do |gfy|
        puts "\t#{gfy.left_fencer.name} vs. #{gfy.right_fencer.name}, gfy: #{gfy.gfycat_gfy_id}"
      end
    end
  end

  desc 'odds of winning when you take the lead'
  task :take_lead, [:weapon] do |t, args|
    require 'sequel'
    ties = Hash.new {|hsh, key| hsh[key] = Array.new([0, 0])}
    Sequel.connect db_address do |db|
      require './models/init'
      Bout.each do |bout|
        if args[:weapon]
          next unless bout.gfycats[0][:weapon ] == args[:weapon]
        end

        # the score was tied at ties[x].  one fencer took the lead.  The number of times they won is in ties[x][0].  The number of times they lost is in ties[x][1]
        bout.gfycats.each do |t|
          # fotl took the lead
          if t.left_score - t.right_score == 1
            side = bout.winner == t.left_fencer_id ? 0 : 1

            ties[t.right_score][side] += 1
          end
          # fotr took the lead
          if t.right_score - t.left_score == 1
            side = bout.winner == t.right_fencer_id ? 0 : 1

            ties[t.left_score][side] += 1
          end
        end
      end

      ties.sort_by{|a, b| a}.each do |k, v|
        puts "When the score is tied at #{k}, the next fencer to score wins #{((v[0].to_f / (v[0] + v[1])).truncate(2) * 100).to_i}% of the time."
      end
    end
  end
end
