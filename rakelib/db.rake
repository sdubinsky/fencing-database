require 'debug'
require_relative './helpers'

namespace :db do
  include Helpers
  desc "Run migrations"
  task :migrate, [:version] do |t, args|
    require "sequel/core"
    Sequel.extension :migration
    version = args[:version].to_i if args[:version]
    Sequel.connect(db_address) do |db|
      puts db_address
      Sequel::Migrator.run(db, "db/migrations", target: version)
    end
  end

  desc "normalize gfycat names"
  task :normalize_names do |t|
    cursor_size = 1000
    start = 0
    normalized = 0
    Sequel.connect(db_address, max_connections: 12, pool_timeout: 20) do |db|
      require './models/init'
      db.extension :pagination
      tid_query = db.select(:tournament_id).from(Gfycat.group_and_count(:tournament_id).where(status: nil)).where{count > 500}
      gfys_dataset = Gfycat.where{
        Sequel.&({left_fencer_id: nil}, {status: nil}, {tournament_id: tid_query})
      }.or{
        Sequel.&({right_fencer_id: nil}, {status: nil}, {tournament_id: tid_query})
      }.reverse(:created_date)
      total = gfys_dataset.count
      if total == 0
        puts "no recent gfys"
        exit
      end

      puts "normalizing #{total} gfys"

      while start < total
        results = Queue.new
        puts "round #{(start/cursor_size.to_f).to_i + 1}/#{(total/cursor_size.to_f + 1).to_i}"
        queue = Queue.new(gfys_dataset.limit(cursor_size).offset(start))

        start += cursor_size
        puts queue.size
        threads = 0.upto(12).map do
          Thread.new do
            begin
              while gfy = queue.pop(non_block=true)
                begin
                  results << gfy.normalize_names
                  print '.'
                rescue => e
                  puts e.message
                end
              end
            rescue ThreadError
            end
          end
        end
        threads.each{|t| t.join}
        gfys = []
        until results.empty?
          gfys << results.pop
        end
        gfys = gfys.compact.reject{|gfy| !gfy[1] || !gfy[2]}

        if gfys.count == 0
          puts "no valid gfys, skipping"
          next
        end

        puts "#{gfys.count} normalized gfys this round"

        normalized += gfys.count
        db.from(Sequel[:gfycats].as(:gfycats),
                db.values(gfys).as(:gs, [:g_id, :left_fencer_id, :right_fencer_id])).
          where{{gs[:g_id]=>gfycats[:id]}}.
          update(left_fencer_id: Sequel[:gs][:left_fencer_id],
                 right_fencer_id: Sequel[:gs][:right_fencer_id],
                 status: 'valid')

        Fencer.where(weapon: nil).where(id: Gfycat.where(weapon: 'epee', status: 'valid').select(:left_fencer_id)).or(id: Gfycat.where(weapon: 'epee', status: 'valid').select(:right_fencer_id)).update(weapon: 'epee')
        Fencer.where(weapon: nil).where(id: Gfycat.where(weapon: 'sabre', status: 'valid').select(:left_fencer_id)).or(id: Gfycat.where(weapon: 'sabre', status: 'valid').select(:right_fencer_id)).update(weapon: 'sabre')
        Fencer.where(weapon: nil).where(id: Gfycat.where(weapon: 'foil', status: 'valid').select(:left_fencer_id)).or(id: Gfycat.where(weapon: 'foil', status: 'valid').select(:right_fencer_id)).update(weapon: 'foil')

        Fencer.
          where(gender: nil).
          where{Sequel[{id: Gfycat.where(gender: 'male', status: 'valid').select(:left_fencer_id)}] |
                Sequel[{id: Gfycat.where(gender: 'male', status: 'valid').
                          select(:right_fencer_id)}]}.
          update(gender: 'male')
        Fencer.
          where(gender: nil).
          where{Sequel[{id: Gfycat.where(gender: 'female', status: 'valid').select(:left_fencer_id)}] |
                Sequel[{id: Gfycat.where(gender: 'female', status: 'valid').
                          select(:right_fencer_id)}]}.
          update(gender: 'female')
      end
      puts "normalized #{normalized} gfys"
    end
  end

  desc "Add new bouts"
  task :add_bouts do |t|
    Sequel.connect db_address do |db|
      require './models/init'
      puts "starting with #{Bout.count} bouts"
      #Update gfys with any bouts that already exist
      db[:gfycats]
        .where(bout_id: nil)
        .update(bout_id: Bout.select(:id)
                  .where(left_fencer_id: Sequel[:gfycats][:left_fencer_id],
                         right_fencer_id: Sequel[:gfycats][:right_fencer_id],
                         tournament_id: Sequel[:gfycats][:tournament_id]))
      #create new bouts from gfys that don't have bouts attached, but that should.  This is defined as gfys that have a left_fencer_id, a right_fencer_id, and a tournament, but no bout.
      Bout.insert([:left_fencer_id, :right_fencer_id,:tournament_id],
                  Gfycat.distinct.select(:left_fencer_id, :right_fencer_id,
                                         :tournament_id)
                    .where(bout_id: nil)
                    .where(Sequel.~(left_fencer_id: nil))
                    .where(Sequel.~(right_fencer_id: nil)))

      #reupdate with new bouts
      db[:gfycats]
        .where(bout_id: nil)
        .update(bout_id: Bout.select(:id)
                  .where(left_fencer_id: Sequel[:gfycats][:left_fencer_id],
                         right_fencer_id: Sequel[:gfycats][:right_fencer_id],
                         tournament_id: Sequel[:gfycats][:tournament_id]))

      #Also add bouts to gfys that have duplicate names
      db[:gfycats]
        .with(:g2, db[:gfycats]
                     .distinct
                     .select(:left_fencer_id, :right_fencer_id,
                             :fotl_name, :fotr_name,
                             :bout_id, :tournament_id)
                     .exclude(left_fencer_id: nil)
                     .exclude(right_fencer_id: nil)
                     .exclude(bout_id: nil))
        .from(:gfycats, :g2)
        .where(Sequel[:gfycats][:tournament_id] => Sequel[:g2][:tournament_id],
               Sequel[:g2][:fotl_name] => Sequel[:gfycats][:fotl_name],
               Sequel[:g2][:fotr_name] => Sequel[:gfycats][:fotr_name])
        .update(bout_id: Sequel[:g2][:bout_id],
                left_fencer_id: Sequel[:g2][:left_fencer_id],
                right_fencer_id: Sequel[:g2][:right_fencer_id])

      db.transaction do
        count = Bout.where(winner: nil).count
        puts "about to set the winner for #{count} bouts"
        queue = Queue.new(Bout.where(winner: nil))
        threads = 0.upto(12).map do
          begin
            while bout = queue.pop(non_block = true)
              begin
                top_gfycat = Gfycat.where(bout_id: bout.id, status: 'valid').
                               select(:gfycat_gfy_id, :left_score, :right_score,
                                      :left_fencer_id, :right_fencer_id,
                                      Sequel.+(:left_score, :right_score).as(:total)).
                               order(:total).
                               reverse.first
                if top_gfycat
                  winner = top_gfycat.left_score > top_gfycat.right_score ? top_gfycat.left_fencer_id : top_gfycat.right_fencer_id
                  bout.winner = winner
                  bout.save
                else
                  puts "no top gfy found for bout #{bout.id}"
                end
              rescue => e
                puts e.message
              end
            end
          rescue ThreadError
          end
        end
        puts "ending with #{Bout.count} bouts"
      end
    end
  end

  desc 'update nationality'
  task :update_nationality do
    Sequel.connect db_address do |db|
      db.extension :pg_array
      require './models/init'
      tournaments = Tournament.all
      tournaments.each do |tournament|
        tournament.urls.each do |url|
          entries = Tournament.download_entries url
          entries.each{|entry| Fencer.where(fie_id: entry[4]).update(nationality: country_code(entry[1]))}
        end
      end
    end
  end

  desc 'add entries for tournaments'
  task :update_entries do
    Sequel.connect db_address do |db|
      db.extension :pg_array
      require './models/init'
      tournaments = Tournament.where(tournament_year: "2024/2025").where{start_time < Time.now.to_i + (60 * 60 * 24 * 8)}.where{start_time > Time.now.to_i}
      tournaments.each do |tournament|
        puts "adding entries for #{tournament.tournament_id}"
        tournament.urls.each do |url|
          entries = Tournament.download_entries url
          puts url
          licenses = entries.map{|entry| entry[4]}
          new_fencers = entries.select{|entry| Fencer.where(fie_id: entry[4]).count == 0}.map do |entry|
            [
              entry[0].split[0],
              entry[0].split[1],
              country_code(entry[1][0..2]),
              entry[6],
              tournament.weapon,
              entry[4],
            ]
          end
          db[:fencers].import([:last_name, :first_name, :nationality, :birthday, :weapon, :fie_id], new_fencers)
          select = Fencer.select(:id, tournament.id).where(fie_id: licenses).exclude(id: FencersTournament.select(:fencer_id).where(tournament_id: tournament.id))
          db[:fencers_tournaments].import([:fencer_id, :tournament_id], select)

          entries.each do |entry|
            seed = entry[2].to_i
            seed = 999 if seed == 0
            fencer = Fencer.first(fie_id: entry[4])
            FencersTournament.first(fencer_id: fencer.id, tournament_id: tournament.id).update(seed: seed)
          end
        end
      end
    end
  end

  desc 'add seeds to entries'
  task :update_seeding do
    Sequel.connect db_address do |db|
      db.extension :pg_array
      require './models/init'
      tournament_ids = FencersTournament.select(:tournament_id).from(FencersTournament.group_and_count(:tournament_id).where(seed: 0)).where{count > 50}
      Tournament.where(id: tournament_ids).each do |tournament|
        puts tournament.tournament_id
        tournament.urls.each do |url|
          entries = Tournament.download_entries(url)
          entries.each do |entry|
            fencer = Fencer.first(fie_id: entry[4])
            seed = entry[2].to_i
            begin
              FencersTournament.first(fencer_id: fencer.id, tournament_id: tournament.id).update(seed: seed)
            rescue => e
              next
            end
          end
        end
      end
    end
  end

  desc 'prep a tournament'
  task :add_tournaments => [:update_entries, :update_seeding] do
  end

  desc 'add gender'
  task :add_gender do
    Sequel.connect db_address do |db|
      db.extension :pg_array
      require './models/init'
      urls = [
        'https://fie.org/competition/2023/797',
        'https://fie.org/competition/2023/801',
        'https://fie.org/competition/2023/800',
        'https://fie.org/competition/2023/1450',
        'https://fie.org/competition/2023/1448',
        'https://fie.org/competition/2023/1452',
        'https://fie.org/competition/2023/1013',
        'https://fie.org/competition/2023/1014',
        'https://fie.org/competition/2023/1011',
        'https://fie.org/competition/2023/586',
        'https://fie.org/competition/2023/584',
        'https://fie.org/competition/2023/588'
      ]
      gender = 'female'
      urls.each do |url|

        entries = Tournament.download_entries(url)
        puts entries.count
        entries.each do |entry|
          fencer = Fencer.first(fie_id: entry[4])
          fencer.gender = gender
          fencer.save
        end
      end
    end
  end

  desc 'update results of tournaments'
  task :update_results do
    require 'pdf-reader'
    require 'open-uri'

    results_suffix = '/results/ranking/pdf'
    Sequel.connect(db_address, max_connections: 5) do |db|
      db.extension :pg_array
      require './models/init'
      queue = Queue.new
      Tournament.where(tournament_year: '2024/2025').where{start_time < Time.now.to_i}.where{start_time > Time.new(2025, 1, 1).to_i}.each do |tournament|
        tournament.urls.map{|a| queue.push([tournament, a])}
      end
      threads = 0.upto(2).map do
        Thread.new do
          until queue.empty?
            tournament, url = queue.pop(non_block = true)
            begin
              pdf = PDF::Reader.new URI.open(url + results_suffix)
            rescue PDF::Reader::MalformedPDFError
              puts "no results page for #{tournament.tournament_id}"
            end
            if !pdf
              puts "no results page for #{tournament.tournament_id}"
              next
            end
            pdf.pages.each do |page|
              page.
                text.
                gsub('\n\n', '\n').
                gsub('\n\n', '\n').
                each_line(chomp: true) do |line|
                line = line.split
                next unless line[0] =~ /\d/
                place = line[0]
                score = line[1]
                next unless score
                name = line[2..-3].join(' ')
                fencer = Fencer.find_name_possibilities(name, tournament.id).first
                if !fencer
                  puts "couldn't find a fencer for #{name} in tournament #{tournament.tournament_id}"
                  next
                end
                entry = FencersTournament.first(tournament_id: tournament.id, fencer_id: fencer.id)

                entry.place = place
                entry.score = score
                entry.save
              end
            end
          end
          print '.'
        end
      end
      threads.each{|t| t.join}
    end
  end

  desc 'score fantasy submissions'
  task :score_submissions do
    Sequel.connect db_address do |db|
      require './models/init'
      FantasySubmission.where(score: nil).each do |submission|
        submission.calculate_score
      end
      FantasySubmission.where(rarity_weighted_score: nil).each do |sub|
        sub.calculate_rarity_weighted_score
      end
    end
  end
end
