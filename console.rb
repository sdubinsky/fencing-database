#!/usr/bin/env ruby

require 'pry'
require 'sequel'
require 'psych'
require 'json'

config = Psych.load_file("./config.yml")

if ENV['DATABASE_URL']
  connstr = ENV['DATABASE_URL']
else
  db_config = config['database']
  if db_config['db_username'] or db_config['db_password']
    login = "#{db_config['db_username']}:#{db_config['db_password']}@"
  else
    login = ''
  end
  connstr = "postgres://#{login}#{db_config['db_address']}/#{db_config['db_name']}"
end
Sequel.extension :core_extensions
DB = Sequel.connect(connstr)
DB.extension(:pagination, :pg_array)
require './models/init'

binding.pry
