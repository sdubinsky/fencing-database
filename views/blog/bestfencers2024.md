# Who's The Best Fencer Of The 2023/2024 Season?

There's a lot of ways to answer the question, but inspired by a question on the GP Fencing discord I'm going to answer it by who won the highest percentage of touches.  What this means is that if you win a bout 15-13, you scored 15 touches of a possible 28, or 53%.  Expanded over a season, this ought to give a good estimate of who was the most dominant fencer.  After all, the fencer who scores the most touches usually wins the bout.  I ran the stats for overall and for top ten fencers in each weapon/gender combo.

Some interesting stuff here!  The dominant athletes of the season are here - Kiefer, Balzer, Kong, but so are a lot of up-and-coming kids who popped off this season.  I think this is an artifact of the bye system.  If you're in the top sixteen, you don't fence the prelim rounds.  If you're close to the top sixteen, you get a relatively easy pool and are more likely to get a bye from that.  If you're just now getting good, you'll have to fence all the prelim rounds but you'll dominate.  In other words, if you're very good but weren't last year you'll have more bouts against people who are much worse than you.

One note on that - I don't have all the prelim rounds recorded, just what was available with an overlay.  I suspect the real data is even more skewed towards up-and-comers.

## Women's Epee

pct    |   Last Name    |   First Name |
-------|----------------|----------------
 0.600 | BABA           | Haruna
 0.593 | XU             | Nuo
 0.588 | CANDASSAMY     | Marie Florence
 0.582 | TANG           | Junyao
 0.578 | HUSISIAN       | Hadley
 0.577 | VARFOLOMYEYEVA | Darja
 0.573 | CHEN           | Chen
 0.571 | KONG           | Man Wai Vivian
 0.568 | MALLO-BRETON   | Auriane
 0.566 | LEHIS          | Katrina

## Men's Epee

pct  | Last Name  |  First Name |
-------|------------|--------------
 0.706 | METRAILLER | Clement
 0.632 | DJATSUK    | Filipp
 0.600 | SVENSSON   | Jonathan
 0.572 | FREILICH   | Yuval Shalom
 0.571 | IMREK      | Samuel
 0.565 | SOMODY     | Soma
 0.564 | CUPR       | Michal
 0.560 | ITO        | Inochi
 0.557 | KANO       | Koki
 0.557 | BOREL      | Yannick

## Women's Foil

pct  |     Last Name     | First Name |
-------|-------------------|------------
 0.613 | POLOZIUK          | Alina
 0.612 | KIEFER            | Lee
 0.607 | SINIGALIA         | Martina
 0.606 | FAVARETTO         | Martina
 0.600 | TAKEZAWA          | Shiori
 0.594 | WALCZYK-KLIMASZYK | Julia
 0.583 | CANDESCU          | Rebeca
 0.581 | MURALIDHARAN      | Mayuri
 0.580 | HARVEY            | Eleanor
 0.576 | ERRIGO            | Arianna

## Men's Foil

pct  | Last Name  |  First Name |
-------|------------|---------------
 0.667 | NISHIGUCHI | Yasutaka
 0.637 | BIANCHI    | Guillaume
 0.636 | BIBARD     | Tyvan
 0.595 | MASSIALAS  | Alexander
 0.584 | HAMZA      | Mohamed
 0.584 | CHOI       | Chun Yin Ryan
 0.578 | ROGER      | Wallerand
 0.576 | ITKIN      | Nick
 0.574 | DOSA       | Daniel
 0.567 | IIMURA     | Kazuki

## Women's Sabre

pct  | Last Name  | First Name |
-------|------------|------------
 0.652 | KATONA     | Renata
 0.646 | BALZER     | Sara
 0.630 | MARTIS     | Sabina
 0.620 | BATTISTON  | Michela
 0.609 | FU         | Ying
 0.609 | CHOI       | Sebeen
 0.592 | NOUTCHA    | Sarah
 0.591 | FOXGITOMER | Chloe
 0.588 | GKOUNTOURA | Theodora
 0.581 | SEO        | Jiyeon

## Men's Sabre

pct  | Last Name  | First Name |
-------|------------|------------
 0.643 | SZATMARI   | Andras
 0.592 | SZILAGYI   | Aron
 0.587 | ARFA       | Fares
 0.586 | GU         | Bongil
 0.579 | ELSISSY    | Ziad
 0.577 | SATTARKHAN | Nazarbay
 0.573 | APITHY     | Bolade
 0.568 | PATRICE    | Sebastien
 0.558 | MARTINE    | Thomas
 0.557 | HEATHCOCK  | Colin
