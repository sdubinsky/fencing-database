# Pick 'Em Fantasy Feb. 9

Almost a full slate of [events](https://fencingdatabase.com/fantasy) this week!  Epee women are in lovely Barcelona, foilists of all stripes are in Turin, and sabre men and women are in Tbilisi and Lima, respectively.  Let's get started!

## Lima

The great tragedy of this Olympic cycle is that only one of the Greek women will make it, despite both being top ten.  Both are great picks for fantasy, as is Olga Kharlan, Manon Apithy-Brunet, or Emura Misaki. With Balzer not in attendance, there's no clear favorite.  I'm going with Georgiadou for my first pick.

For the next set, Mormile, Bashta, and Berder are three consistent high performers, but I'm going with Erbil following her hot performance in Tunis.

Nazlymov is as always the cynic's pick, but anyone in the third set can make a run.  I'm going with Maxwell as a sign of British fencing's sudden surge.  If only they had done it last year, they'd be in great shape for the Olympics.

## Tbilisi

Right now in men's sabre, there's Bazadze, there's Szilagyi, and everyone else.  They don't always win but they're always the favorites.  I'm going with Bazadze on the assumption that old man Szilagyi is saving his strength for his astounding fifth Olympics.

Everyone in the second set is good, but there are two standouts here too.  Colin Heathcock fresh off a phenomenal result in Tunis, or Gu Bongil making one last run.  Heathcock for me, for no particular reason.

The third set is closer.  I'm getting good vibes from Di Tella, Yildirim, and Ha.  I think I'm going with Yildirim, but anyone here can do well.

## Turin Women

In foil especially, you can never count out the Italians, and there are three in the first set to choose from.  I'm going with Italian slayer Lee Kiefer, but if you're Canadian, German, Japanese or French feel free to be a homer.

I've been a huge Errigo fan ever since she tried to make the Olympics in two weapons last cycle, so I'm picking her for sure.  I don't think any of the Americans in this set can quite make it, but Cipressa and Ranvier are good choices too.

Third set is, as usual, tight.  I think Blaze, Berthier, and Pasztor stand out for me, and I'm picking Blaze.

## Turin Men

In foil especially, you can never count out the Italians, except in men's foil, where the Americans are at least as dominant.  I'm going with long-armed Marini, but anyone here is a good choice.

Pretty clear split in the second set to me, where the top half is noticeably stronger than the bottom half.  Plus, if you pick someone in the top 16, you're guaranteed top-64 points because they get a bye.  Chamley-Watson for me.

Rzadkowski is a new name to me, and I'm curious to see how well he does. Iimura, Lee, and Avola are the safe names here, but I'm going to take a risk on someone new.

## Barcelona

Crazy stuff in Doha, huh?  Comebacks, upsets, and overtimes everywhere.  Through it all Kong retained her #1 spot, and after a performance like that I can't not pick her.  If we've learned anything about women's epee, though, it's that nobody is safe.  Everyone in the top ten is a threat.

Kaylin Hsieh rewarded my faith in her last week, so I'm going to pick her again.  Xiao seems to have fallen back to Earth a bit, but Ndolo, Fiamingo, and last week's runner-up Rizzi are all good picks.

For the last set, Isola and Embrich are pretty clear favorites.  They were sixth and thirteenth, respectively, in Doha.  Epee being what it is, anyone can pop off, but I'm going with Isola.

Get your picks in [here](https://fencingdatabase.com/fantasy), and good luck!
