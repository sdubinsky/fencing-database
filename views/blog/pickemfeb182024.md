# Pick 'Em Fantasy Feb. 18

Another week, and I maintain my place at the top of the [overall rankings](https://www.fencingdatabase.com/fantasy).  [Olympic Foil](https://www.youtube.com/c/olympicfoil) can try all he wants, he just doesn't have what it takes!  Although this is a foil heavy week, so he might.  That aside, it's a relatively quiet week this go 'round.  Foilists are off to Cairo, and epee men are in Heidenheim, but everyone else gets a break.  The sabre men especially could use some time off to cool down.  On to the picks!

## Cairo Men

The impression I'm getting is that the top ten in MF are the top ten for a reason.  Perhaps you're going to ride Ka Long's hot hand, or you think Marini is going to bounce back after a relatively early exit in Turin, or you want Hamza's home field advantage.  Personally, I'm going with Meinhardt.  His wife's coach is from Egypt, that must mean something.

Garozzo lost to the eventual champ and you should never count an Italian out in foil, but Choupenitch and Llavador made it farther.  Choupenitch's result gets him in the top 16, so I'm going with him.  Plus, a silver medal has to mean something.

I'm a bit weak on foilists this deep into the seedings, but Iimura made top eight and I like that.

## Cairo Women

Lee Kiefer is once again the fencer to beat.  Coming in to what might be her last Olympics, she definitely wants to go out with a bang.  The rest of the top ten are all good, but she's the clear favorite.

Dare I try for a 2/3 USA ballot?  I don't think so.  I think Frenchwoman Ranvier will pop off, although I reserve the right to say "I told you so" if Weintraub or Scruggs do well.

None of the final set seem like fencers who can or will get upsets to me.  I'm going with Anita Blaze, but everyone here will probably get about the same number of points

## Heidenheim

With a shocking win in Doha, Israeli Freilich is in the driver's seat for the European spot in Paris, and might have already locked it up.  If he can keep his foot on the gas all the way, he's unbeatable right now.  That aside, there were a truly insane number of upsets in Doha.  Anyone here is good.

The second set is equally good.  Do you want 2019 world champ Siklosi?  2012 gold medalish Limardo Gascon?  Jiri Beran is 42 fucking years old and still in the top 20!  I'm going with Siklosi.  Good luck.

French wunderkind Billa has fallen off some since his performance in Budapest last year, but this might be his time to shine.  Lots of other good names here, but the rule is always if you can pick Borel outside the top ten, pick Borel.  So I'm picking Borel.

Get your [picks](https://www.fencingdatabase.com/fantasy) in by Thursday, and good luck!
