# Pick 'Em Fantasy Doha Epee

I'm trying out a new feature where I recommend picks for [fantasy fencing](/fantasy).  I make no guarantees on the accuracy of my picks; on the other hand, I **am** at the top of the leaderboard.  This week there are only two events, Doha men and Doha women.  Let's get started!

## Doha Men

### 1-10

Just close your eyes and pick someone.  This top ten is stacked.  I personally am going with Kano, but any of Cannone, Di Veroli, Siklosi, or [Minobe](https://www.youtube.com/watch?v=KwwPDH61E0o) are safe bets too.  Koch won champs last year.

### 11-20

I'm torn here between Bayard and Komata, both of whom I have good feelings about.  Bayard did better in Vancouver, so I'm going with him.  Other options that jump out at me are recent top-tenner Rodriguez,  Pereira, and Limardo Gascon.  Elsayed is the cynical pick.  Top tip for this row - the first six are guaranteed a spot in the 64, so that's free points.

### 21-30

I'd pick [Borel](https://www.youtube.com/watch?v=8KhNBsNpZn8) if he was in the 1-10 slot.  Having him all the way down here is a steal.  Other options: Vancouver champ Midelton, Israeli phenom Cohen, another Limardo.

## Doha Women

### 1-10

I'm a huge [Moellhausen](https://www.youtube.com/watch?v=Av04zK8Ed54) fan.  Song fences like she has an extra inch on her French grip, Candassamy won Worlds, and Fiamingo makes it work despite being a foot shorter than everyone else.

### 11-21

Xiao shone in her hometown event, taking names all through Vancouver, but I wasn't extremely impressed with her fencing.  I'm going to take a swerve here and go with Ndolo.  Holmes in one last hurrah is another good option.

### 21-30

[Hsieh](https://www.youtube.com/@kaylinhsieh) posts some fun vlogs, and I like her fencing.  Kirpu is underrated for this slot, although not as badly as Borel, and everyone else is about the same level - a threat, but not a big one.

For reference, thats: Kano, Bayard, Borel, Moellhausen, Ndolo, Hsieh for me.  Good luck to everyone!
